# README #

### What is this repository for? ###

* Visualise visitor numbers per day by RTO (and origin) so anomalies are visible and the impact of events can be assessed. Also a face validity check on data quality.

### How do I get set up? ###

* Run validation.py in Python 3 having commented different settings for filtered_regions and reports_dets in and out. To add more events edit conf.py before running again. Runs off latest version of arrivals_and_visits.db sqlite database from John Graves.

### Who do I talk to? ###

* Grant Paton-Simpson (Grant.PatonSimpson@qrious.co.nz)