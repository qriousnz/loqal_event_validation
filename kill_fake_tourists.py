#! /usr/bin/env python3

"""
We have a month-long step-change for (fake) tourists going from Southland to,
yep, Northland. It is pronounced in Day Trips but, fortunately for detection
purposes, also in Visits.

...
"33"
"31"
"36"
"46"
--------
"481"
"508"
"439"
...
"162"
"159"
"145"
--------
"16"
"16"
"24"

Day Trips involves removing commuters and is more complicated to work out why a
metric is what it is. But for Visits all we need to know is home region (e.g.
Southland) and destination (e.g. Northland). We can actually look at the IMSI's
concerned and thus the cell_id_rti's. Any cell_id_rti's deemed to be in
Southland/Northland but actually in the other?

first_in_day_categorized has imsi, date, dest, origin so we can filter to those
with dest of, say, Northland, and an origin of, say, Southland, and look at the
cell id's they connect to. How many of those IMSI's seem to be in Northland a
month earlier when they are supposed to live in Southland?

2016-04-06 has a high number of alleged Southlanders going to Northland. Surely
some of them are actually Northlanders as evidenced by the number of Northland
cell_id_rti's they connect to.
"""

from collections import Counter, defaultdict
from itertools import count
import json
from pprint import pprint as pp

import pandas as pd

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, dates, db, spatial, utils

LATEST_CSM_PKL = 'csm_s2_12_maps_20170708.pkl'
S2_12_FREQS_BY_DATE_JSON = 's2_12_index_freqs_by_date.json'
FAKE_TOURIST_CELLS_JSON = 'fake_tourist_cells.json'

FIRST_IN_DAY_ROOT_KEY = 'data'
FIRST_IN_DAY_CAT_ROOT_KEY = 'matched'
MAX_BY_IMSI_ROOT_KEY = 'max_by_imsi'

index2grid12 = {0: 89042224, 1: 89041561, 2: 89041530, 3: 89041527, 4: 89041476,
5: 89041436, 6: 89041379, 7: 89041233, 8: 89041171, 9: 89041137, 10: 89041136,
11: 89041132, 12: 89041131, 13: 89041127, 14: 89041112, 15: 89040639, 16:
89040381, 17: 89040370, 18: 89040072, 19: 89039866, 20: 89039727, 21: 89038960,
22: 89038770, 23: 89038232, 24: 89038181, 25: 89038114, 26: 89037696, 27:
89037623, 28: 89037469, 29: 89037402, 30: 89037079, 31: 89036997, 32: 89036879,
33: 89036807, 34: 89036674, 35: 89036524, 36: 89036516, 37: 89036453, 38:
89036427, 39: 89036416, 40: 89036329, 41: 89036327, 42: 89036326, 43: 89036325,
44: 89036324, 45: 89036322, 46: 89036317, 47: 89036314, 48: 89036239, 49:
89025226, 50: 88175989, 51: 88175988, 52: 88175976, 53: 88175971, 54: 88175970,
55: 88175968, 56: 88175967, 57: 88175964, 58: 88175949, 59: 88175944, 60:
88173983, 61: 88173052, 62: 88173005, 63: 88172876, 64: 88172508, 65: 88172169,
66: 88172156, 67: 88172155, 68: 88172124, 69: 88171697, 70: 88171292, 71:
88171133, 72: 88171085, 73: 88171048, 74: 88171001, 75: 88170668, 76: 88170591,
77: 88170539, 78: 88170537, 79: 88170521, 80: 88170513, 81: 88170507, 82:
88170024, 83: 88169578, 84: 88169245, 85: 88168982, 86: 88168826, 87: 88168767,
88: 88168730, 89: 88168387, 90: 88168207, 91: 88168104, 92: 88167859, 93:
88167418, 94: 88167036, 95: 88167020, 96: 88166526, 97: 88166190, 98: 88166176,
99: 88165604, 100: 88165587, 101: 88165581, 102: 88165563, 103: 88165533, 104:
88165334, 105: 88165312, 106: 88165094, 107: 88165030, 108: 88165009, 109:
88164997, 110: 88164970, 111: 57383030, 112: 57383023, 113: 57382807, 114:
57382795, 115: 57382790, 116: 57382495, 117: 57382492, 118: 57382475, 119:
57382474, 120: 57382471, 121: 57382448, 122: 57382409, 123: 57382382, 124:
57382346, 125: 57382323, 126: 57382318, 127: 57382299, 128: 57382285, 129:
57382265, 130: 57382251, 131: 57382250, 132: 57382247, 133: 57382246, 134:
57382234, 135: 57382232, 136: 57382227, 137: 57382085, 138: 57381901, 139:
57381897, 140: 57381875, 141: 57381843, 142: 57381807, 143: 57381804, 144:
57381769, 145: 57381750, 146: 57381742, 147: 57381740, 148: 57381613, 149:
57381576, 150: 57381421, 151: 57381420, 152: 57381399, 153: 57375649, 154:
57374939, 155: 57374861, 156: 57374813, 157: 57374797, 158: 57374782, 159:
57374778, 160: 57374777, 161: 57374723, 162: 57374713, 163: 57374647, 164:
57374594, 165: 57374591, 166: 57374327, 167: 57374146, 168: 57373999, 169:
57373976, 170: 57373938, 171: 57373933, 172: 57373925, 173: 57373918, 174:
57373729, 175: 57373672, 176: 57373658, 177: 57373644, 178: 57373639, 179:
57373624, 180: 57373618, 181: 57373617, 182: 57373484, 183: 57373423, 184:
57373362, 185: 57372522, 186: 57372518, 187: 57372405, 188: 57372383, 189:
57372382, 190: 57372381, 191: 57372380, 192: 57372379, 193: 57372377, 194:
57372373, 195: 57372372, 196: 57372371, 197: 57372370, 198: 57372364, 199:
57372361, 200: 57372358, 201: 57372352, 202: 57372319, 203: 57372301, 204:
57372279, 205: 57372244, 206: 57372205, 207: 57372200, 208: 57372169, 209:
57372164, 210: 57372047, 211: 57371964, 212: 57371940, 213: 57371911, 214:
57371864, 215: 57371844, 216: 57371821, 217: 57371818, 218: 57371797, 219:
57371790, 220: 57371754, 221: 57371736, 222: 57371685, 223: 57371674, 224:
57371619, 225: 57371542, 226: 57371426, 227: 57371394, 228: 57371311, 229:
57371296, 230: 57371277, 231: 57371265, 232: 57371263, 233: 57371181, 234:
57371140, 235: 57371109, 236: 57371101, 237: 57371095, 238: 57371082, 239:
57371079, 240: 57371039, 241: 57371018, 242: 57371010, 243: 57370964, 244:
57370930, 245: 57370920, 246: 57370913, 247: 57370909, 248: 57370905, 249:
57370903, 250: 57370900, 251: 57370899, 252: 57370898, 253: 57370897, 254:
57370896, 255: 57370895, 256: 57370877, 257: 57370874, 258: 57370873, 259:
57370870, 260: 57370869, 261: 57370864, 262: 57370863, 263: 57370861, 264:
57370858, 265: 57370851, 266: 57370849, 267: 57370826, 268: 57370825, 269:
57370823, 270: 57370822, 271: 57370821, 272: 57370820, 273: 57370819, 274:
57370810, 275: 57370807, 276: 57370698, 277: 57370678, 278: 57370667, 279:
57370646, 280: 57370644, 281: 57370603, 282: 57370592, 283: 57370549, 284:
57370469, 285: 57370448, 286: 57370423, 287: 57370390, 288: 57370377, 289:
57370369, 290: 57370362, 291: 57370328, 292: 57370117, 293: 57370072, 294:
57370050, 295: 57369944, 296: 57369939, 297: 57369899, 298: 57369839, 299:
57369769, 300: 57369480, 301: 57369432, 302: 57369388, 303: 57369251, 304:
57369234, 305: 57369215, 306: 57369122, 307: 57369103, 308: 57369101, 309:
57369045, 310: 57368915, 311: 57368893, 312: 57368891, 313: 57368890, 314:
57368888, 315: 57368887, 316: 57368882, 317: 57368881, 318: 57368715, 319:
57368650, 320: 57368528, 321: 57368525, 322: 57368524, 323: 57368478, 324:
57368453, 325: 57368448, 326: 57368342, 327: 57368027, 328: 57368022, 329:
57367747, 330: 57367653, 331: 57367064, 332: 57367045, 333: 57367040, 334:
57366754, 335: 57366687, 336: 57366540, 337: 57366526, 338: 57366517, 339:
57366513, 340: 57366397, 341: 57366396, 342: 57366337, 343: 57366032, 344:
57365846, 345: 57365823, 346: 57365434, 347: 57365370, 348: 57365357, 349:
57365307, 350: 57365238, 351: 57364244, 352: 57364190, 353: 57364056, 354:
57364054, 355: 57364051, 356: 57364050, 357: 57364049, 358: 57364048, 359:
57364046, 360: 57364042, 361: 57363974, 362: 57363948, 363: 57363901, 364:
57363877, 365: 57363875, 366: 57363873, 367: 57363867, 368: 57363865, 369:
57363862, 370: 57363847, 371: 57363815, 372: 57363783, 373: 57362526, 374:
57362274, 375: 57361611, 376: 57361596, 377: 57361001, 378: 57360998, 379:
57360997, 380: 57360981, 381: 57360813, 382: 57360796, 383: 57360755, 384:
57357256, 385: 57357250, 386: 57357249, 387: 57357247, 388: 57357244, 389:
57357232, 390: 57356463, 391: 57356238, 392: 57356031, 393: 57355798, 394:
57354243, 395: 57352833, 396: 57352656, 397: 57293527, 398: 57293526, 399:
57293488, 400: 57293483, 401: 57293482, 402: 57293478, 403: 57293476, 404:
57293368, 405: 57293163, 406: 57286249, 407: 57283493, 408: 57283074, 409:
57282789, 410: 57282490, 411: 57282412, 412: 57282290, 413: 57282261, 414:
57282217, 415: 57282001, 416: 57281978, 417: 57281954, 418: 57281945, 419:
57281944, 420: 57281943, 421: 57281942, 422: 57281941, 423: 57281940, 424:
57281819, 425: 57281772, 426: 57281698, 427: 57281293, 428: 57281066, 429:
57280826, 430: 57280778, 431: 57280777, 432: 57280774, 433: 57280762, 434:
57280747, 435: 57280706, 436: 57280545, 437: 57280445, 438: 57280421, 439:
57280207, 440: 57280153, 441: 57280149, 442: 57280141, 443: 57280076, 444:
57280042, 445: 57279974, 446: 57279929, 447: 57279887, 448: 57279853, 449:
57279842, 450: 57279835, 451: 57279833, 452: 57279826, 453: 57279804, 454:
57279763, 455: 57279760, 456: 57279757, 457: 57279754, 458: 57279752, 459:
57279751, 460: 57279680, 461: 57279646, 462: 57279622, 463: 57279618, 464:
57279589, 465: 57279568, 466: 57279299, 467: 57279217, 468: 57279126, 469:
57279083, 470: 57279082, 471: 57279081, 472: 57279078, 473: 57279039, 474:
57279003, 475: 57278937, 476: 57278921, 477: 57278916, 478: 57278892, 479:
57278891, 480: 57278792, 481: 57278671, 482: 57278495, 483: 57278494, 484:
57278493, 485: 57278488, 486: 57278482, 487: 57278476, 488: 57278473, 489:
57277112, 490: 57277101, 491: 57277095, 492: 57277092, 493: 57277088, 494:
57277084, 495: 57277083, 496: 57277078, 497: 57275980, 498: 57272138, 499:
57271414, 500: 57271412, 501: 57271309, 502: 57271238, 503: 57270251, 504:
57270160, 505: 57270151, 506: 57270122, 507: 57270120, 508: 57270119, 509:
57270118, 510: 57270112, 511: 57269524, 512: 57269471, 513: 57269438, 514:
57269410, 515: 57269408, 516: 57269402, 517: 57269401, 518: 57269400, 519:
57269376, 520: 57269302, 521: 57269288, 522: 57269238, 523: 57268321, 524:
57268167, 525: 57267843, 526: 57265680, 527: 57265482, 528: 57264974, 529:
57264970, 530: 57264854, 531: 57264635, 532: 57264590, 533: 57264546, 534:
57264506, 535: 57264292, 536: 57264285, 537: 57264282, 538: 57264270, 539:
57264201, 540: 57263720, 541: 57263573, 542: 57263503, 543: 57263502, 544:
57263495, 545: 57263490, 546: 57263489, 547: 57263488, 548: 57263487, 549:
57263486, 550: 57263485, 551: 57263484, 552: 57263483, 553: 57263480, 554:
57263475, 555: 57263473, 556: 57263472, 557: 57263470, 558: 57263469, 559:
57263468, 560: 57263463, 561: 57263462, 562: 57263458, 563: 57263454, 564:
57263453, 565: 57263452, 566: 57263447, 567: 57263444, 568: 57263443, 569:
57263442, 570: 57263441, 571: 57263439, 572: 57263436, 573: 57263428, 574:
57263403, 575: 57263402, 576: 57263370, 577: 57260724, 578: 57253472, 579:
57250196, 580: 57250113, 581: 57250112, 582: 57250110, 583: 57250107, 584:
57250095, 585: 57250053, 586: 57250052, 587: 57250051, 588: 57250049, 589:
57250048, 590: 57250047, 591: 57250039, 592: 57250033, 593: 57250032, 594:
57250014, 595: 57249987, 596: 57249884, 597: 57249878, 598: 57249824, 599:
57249818, 600: 57249817, 601: 57249806, 602: 57249729, 603: 57249709, 604:
57249708, 605: 57249707, 606: 57249706, 607: 57249705, 608: 57249703, 609:
57249701, 610: 57249658, 611: 57249649, 612: 57249645, 613: 57249641, 614:
57249628, 615: 57249607, 616: 57249591, 617: 57249586, 618: 57249571, 619:
57249258, 620: 57249241, 621: 57249171, 622: 57249049, 623: 57248957, 624:
57248949, 625: 57248932, 626: 57248923, 627: 57248915, 628: 57248908, 629:
57248904, 630: 57248889, 631: 57248883, 632: 57248881, 633: 57248869, 634:
57248867, 635: 57248865, 636: 57248862, 637: 57248861, 638: 57248860, 639:
57248859, 640: 57248858, 641: 57248857, 642: 57248856, 643: 57248855, 644:
57248854, 645: 57248853, 646: 57248852, 647: 57248851, 648: 57248850, 649:
57248849, 650: 57248848, 651: 57248847, 652: 57248845, 653: 57248844, 654:
57248843, 655: 57248842, 656: 57248841, 657: 57248840, 658: 57248839, 659:
57248837, 660: 57248836, 661: 57248833, 662: 57248832, 663: 57247512, 664:
57247257, 665: 57247227, 666: 57247107, 667: 57246822, 668: 57246500, 669:
57246409, 670: 57246364, 671: 57245682, 672: 57244549, 673: 57244545, 674:
57244531, 675: 57244497, 676: 57244491, 677: 57244436, 678: 57244158, 679:
57244027, 680: 57243955, 681: 57243826, 682: 57243818, 683: 57243731, 684:
57242445, 685: 57242292, 686: 57241843, 687: 57241747, 688: 57241682, 689:
57241657, 690: 57241550, 691: 57241477, 692: 57241453, 693: 57241362, 694:
57241259, 695: 57241217, 696: 57241098, 697: 57241086, 698: 57241054, 699:
57241053, 700: 57241047, 701: 57241012, 702: 57240931, 703: 57240723, 704:
57239513, 705: 57239457, 706: 57239410, 707: 57239376, 708: 57239103, 709:
57239057, 710: 57239055, 711: 57239050, 712: 57239042, 713: 57239028, 714:
57238960, 715: 57238685, 716: 57238543, 717: 57238532, 718: 57238195, 719:
57238080, 720: 57237864, 721: 57237166, 722: 57236888, 723: 57236395, 724:
57235584, 725: 57235200, 726: 57235071, 727: 57234010, 728: 57233761, 729:
57231888, 730: 57231717, 731: 57223996, 732: 57223423, 733: 57223379, 734:
57223216, 735: 57222581, 736: 57191948, 737: 57191916, 738: 57191871, 739:
57191842, 740: 57191806, 741: 57191759, 742: 57191707, 743: 57191616, 744:
57191343, 745: 57191339, 746: 57191286, 747: 57191138, 748: 57190476, 749:
57190467, 750: 57190463, 751: 57190276, 752: 57189801, 753: 57189665, 754:
57189057, 755: 57189056, 756: 57189049, 757: 57189028, 758: 57189019, 759:
57189008, 760: 57189006, 761: 57188999, 762: 57188994, 763: 57188980, 764:
57188979, 765: 57188978, 766: 57188977, 767: 57188976, 768: 57188968, 769:
57188951, 770: 57188948, 771: 57186707, 772: 57185971, 773: 57185952, 774:
57185757, 775: 57185702, 776: 57185626, 777: 57185623, 778: 57185620, 779:
57185617, 780: 57185614, 781: 57185597, 782: 57185588, 783: 57185584, 784:
57174909, 785: 57174872, 786: 57174868, 787: 57174839, 788: 57174727, 789:
57174712, 790: 57174691, 791: 57174690, 792: 57174681, 793: 57174678, 794:
57174676, 795: 57174675, 796: 57174674, 797: 57174672, 798: 57174655, 799:
57174652, 800: 57174651, 801: 57174650, 802: 57174648, 803: 57174646, 804:
57174645, 805: 57174644, 806: 57174643, 807: 57174642, 808: 57174641, 809:
57174640, 810: 57174639, 811: 57174638, 812: 57174637, 813: 57174636, 814:
57174635, 815: 57174633, 816: 57174632, 817: 57174631, 818: 57174630, 819:
57174629, 820: 57174628, 821: 57174627, 822: 57174626, 823: 57174625, 824:
57174624, 825: 57174623, 826: 57174621, 827: 57174620, 828: 57174619, 829:
57174618, 830: 57174617, 831: 57174616, 832: 57174615, 833: 57174612, 834:
57174609, 835: 57174608, 836: 57174607, 837: 57174606, 838: 57174605, 839:
57174604, 840: 57174603, 841: 57174602, 842: 57174601, 843: 57174600, 844:
57174599, 845: 57174598, 846: 57174597, 847: 57174596, 848: 57174595, 849:
57174594, 850: 57174593, 851: 57174592, 852: 57174591, 853: 57174590, 854:
57174589, 855: 57174588, 856: 57174587, 857: 57174585, 858: 57174584, 859:
57174583, 860: 57174582, 861: 57174581, 862: 57174580, 863: 57174579, 864:
57174578, 865: 57174577, 866: 57174576, 867: 57174574, 868: 57174561, 869:
57174560, 870: 57174559, 871: 57174558, 872: 57174543, 873: 57174542, 874:
57174541, 875: 57174539, 876: 57174538, 877: 57174537, 878: 57174536, 879:
57174535, 880: 57174534, 881: 57174531, 882: 57174530, 883: 57174529, 884:
57174528, 885: 57174527, 886: 57174526, 887: 57174525, 888: 57174524, 889:
57174520, 890: 57174518, 891: 57174517, 892: 57174515, 893: 57174512, 894:
57174503, 895: 57174501, 896: 57174495, 897: 57174494, 898: 57174493, 899:
57174492, 900: 57174488, 901: 57174487, 902: 57174486, 903: 57174482, 904:
57174481, 905: 57174480, 906: 57174479, 907: 57174478, 908: 57174477, 909:
57174476, 910: 57174473, 911: 57174472, 912: 57174471, 913: 57174470, 914:
57174467, 915: 57174466, 916: 57174465, 917: 57174463, 918: 57174462, 919:
57174460, 920: 57174392, 921: 57174326, 922: 57174312, 923: 57174311, 924:
57174304, 925: 57174300, 926: 57174296, 927: 57174278, 928: 57174247, 929:
57174231, 930: 57174210, 931: 57174178, 932: 57174173, 933: 57174145, 934:
57174122, 935: 57173904, 936: 57173829, 937: 57173774, 938: 57173760, 939:
57173752, 940: 57173740, 941: 57173729, 942: 57173726, 943: 57173634, 944:
57173223, 945: 57173166, 946: 57173117, 947: 57173100, 948: 57173098, 949:
57173019, 950: 57172994, 951: 57172945, 952: 57172892, 953: 57172738, 954:
57172714, 955: 57172180, 956: 57172079, 957: 57172041, 958: 57171777, 959:
57171672, 960: 57171524, 961: 57171471, 962: 57171467, 963: 57171446, 964:
57171442, 965: 57171428, 966: 57171414, 967: 57171410, 968: 57171391, 969:
57171383, 970: 57171245, 971: 57171243, 972: 57171144, 973: 57170946, 974:
57170935, 975: 57170934, 976: 57170930, 977: 57170886, 978: 57170884, 979:
57170854, 980: 57170807, 981: 57170735, 982: 57168229, 983: 57168224, 984:
57167442, 985: 57167308, 986: 57167059, 987: 57166902, 988: 57166819, 989:
57166767, 990: 57166757, 991: 57166756, 992: 57166359, 993: 57368579, 994:
57372404, 995: 57370827, 996: 57381936, 997: 57170719, 998: 57271372, 999:
89036821, 1000: 57190407, 1001: 88165903, 1002: 89041432, 1003: 57188993, 1004:
57266794, 1005: 57235196, 1006: 57363727, 1007: 57241600}

def hdf5_to_csv(hdf5_fpath, root_key, metadata_only=False):
    """
    root_key -- an internal key contained in the HDF5 file.

    Returns csv_fpath, sql_tpl, tblname
    """
    sql_make_first_in_day_tpl = """\
    CREATE TABLE {tblname} (
      id integer,
      imsi_rti bigint,
      s2_12_index integer,
      PRIMARY KEY(id)
    )
    """
    sql_make_first_in_day_categorized_tpl = """\
    CREATE TABLE {tblname} (
      id integer,
      imsi_rti bigint,
      s2_12_index integer,
      prior_s2_12_index double precision,
      staying_from_prior boolean,
      s2_12_home integer,
      mcc integer,
      week_first_seen date,
      local_sim_flag boolean,
      at_home boolean,
      is_domestic boolean,
      category double precision,
      PRIMARY KEY(id)
    )
    """
    sql_make_max_by_imsi_35_days_tpl = """\
    CREATE TABLE {tblname} (
      id integer,
      imsi_rti bigint,
      s2_12_index integer,
      count integer,
      PRIMARY KEY(id)
    )
    """
    csv_fpath = "{}.csv".format(hdf5_fpath)
    datestr = hdf5_fpath[-8:]
    tblname = "{}_{}".format(root_key, datestr)
    fname = os.path.split(hdf5_fpath)[1]
    if fname.startswith('first_in_day_categorized_'):
        sql_tpl = sql_make_first_in_day_categorized_tpl
    elif fname.startswith('first_in_day_'):
        sql_tpl = sql_make_first_in_day_tpl
    elif fname.startswith(''):
        sql_tpl = sql_make_max_by_imsi_35_days_tpl
    else:
        raise Exception("Unexpected fname '{}'".format(fname))
    if not metadata_only:
        try:
            store = pd.HDFStore(hdf5_fpath)
        except OSError:
            print("Does hdf5_fpath '{}' exist?".format(hdf5_fpath))
            raise Exception
        try:
            data = store[root_key]
        except KeyError:
            print("Is '{}' a key within '{}'?".format(root_key, hdf5_fpath))
            raise Exception
        data.to_csv(csv_fpath, header=False)
        store.close()
    return csv_fpath, sql_tpl, tblname

def make_csvs(min_daily_date_str, max_daily_date_str, min_weekly_date_str,
        max_weekly_date_str, root, metadata_only=False):
    """
    date_strs -- '2017-09-23' even though we strip the hyphens out to match
    John's convention.
    """
    csvs_dets = []
    daily_date_strs = dates.Dates.date_strs_from_min_max_date_strs(
        min_daily_date_str, max_daily_date_str)
    for daily_date_str in daily_date_strs:
        daily_date_str = daily_date_str.replace('-', '')
        first_in_day = '{}/first_in_day_{}'.format(root, daily_date_str)
        csv_dets = hdf5_to_csv(hdf5_fpath=first_in_day,
            root_key=FIRST_IN_DAY_ROOT_KEY, metadata_only=metadata_only)
        csvs_dets.append(csv_dets)
        first_in_day_cat = '{}/first_in_day_categorized_{}'.format(root,
            daily_date_str)
        csv_dets = hdf5_to_csv(hdf5_fpath=first_in_day_cat,
            root_key=FIRST_IN_DAY_CAT_ROOT_KEY, metadata_only=metadata_only)
        csvs_dets.append(csv_dets)
        if not metadata_only:
            print("Completed making daily csv's for {}".format(daily_date_str))
    weekly_date_strs = dates.Dates.date_strs_from_min_max_date_strs(
        min_weekly_date_str, max_weekly_date_str)
    for weekly_date_str in weekly_date_strs:
        ## max_by_imsi is weekly so only 1 in 7 should exist
        max_by_imsi_35_days = '{}/max_by_imsi_35_days_ending_{}'.format(root,
            weekly_date_str)
        if os.path.exists(max_by_imsi_35_days):
            csv_dets = hdf5_to_csv(hdf5_fpath=max_by_imsi_35_days,
                root_key=MAX_BY_IMSI_ROOT_KEY, metadata_only=metadata_only)
            csvs_dets.append(csv_dets)
            if not metadata_only:
                print("Completed making weekly csv for {}".format(
                    weekly_date_str))
    return csvs_dets

def csv2pg(con_local, cur_local, csv_fpath, sql_tpl, tblname,
        force_fresh=False):
    table_exists = db.Pg.table_exists(cur_local, tblname)
    if not table_exists or (table_exists and force_fresh):
        db.Pg.drop_tbl(con_local, cur_local, tblname)
        cur_local.execute(sql_tpl.format(tblname=tblname))
        con_local.commit()
        with open(csv_fpath) as f:
            cur_local.copy_from(f, tblname, sep=',')
        con_local.commit()
        print("Just made fresh version of {}".format(tblname))
    else:
        print("Not making {} because it already exists and force_fresh is not "
            "set to True".format(tblname))

def make_src_data(con_local, cur_local, min_daily_date_str, max_daily_date_str,
        min_weekly_date_str, max_weekly_date_str,
        make_fresh_csv=True, make_fresh_pg=True):
    utils.warn("Assumes all the HDF5 files used as the original source have "
        "been transferred from the server to the local machine")
    if not make_fresh_csv:
        utils.warn("Not making fresh CSV data from HDF5 files")
    csvs_dets = make_csvs(min_daily_date_str, max_daily_date_str,
        min_weekly_date_str, max_weekly_date_str,
        root=os.path.join(conf.VALIDATION_ROOT, 'storage'),
        metadata_only=not make_fresh_csv)
    idx_tblname = 2
    tblnames = [tup[idx_tblname] for tup in csvs_dets]
    if make_fresh_pg:
        for csv_fpath, sql_tpl, tblname in csvs_dets:
            csv2pg(con_local, cur_local, csv_fpath, sql_tpl, tblname,
                force_fresh=False)
    else:
        utils.warn("Not making fresh PG data from CSV files")
    if make_fresh_csv or make_fresh_pg: print("FINISHED! Made all source data!")
    return tblnames

def _get_imsi_dets(cur_local, tblname, src_region, dest_region,
        s2_12_to_region_name):
    """
    Collect all IMSI's which had s2_12_home in origin_region. Also return
    numbers of IMSI's per s2_12 grid within the source region.
    """
    debug = False
    sql = """\
    SELECT *
    FROM {}
    """.format(tblname)
    cur_local.execute(sql)
    data = cur_local.fetchall()
    src_imsis = []
    src2dest_imsis = []
    s2_12_indexes_src = []
    for row in data:
        src_index = row['s2_12_home']
        src_reg_derived = s2_12_to_region_name.get(src_index)
        dest_index = row['s2_12_index']
        dest_reg_derived = s2_12_to_region_name.get(dest_index)
        if debug:
            print(src_index, src_reg_derived)
            print(dest_index, dest_reg_derived)
        if src_reg_derived == src_region:
            imsi = row['imsi_rti']
            src_imsis.append(imsi)
            s2_12_indexes_src.append(src_index)
            if dest_reg_derived == dest_region:
                src2dest_imsis.append(imsi)
    s2_12_index_src_breakdown = Counter(s2_12_indexes_src)
    return src_imsis, src2dest_imsis, s2_12_index_src_breakdown

def report_src_region_numbers(cur_local, src_region, dest_region,
        s2_12_to_region_name, tblnames, create_fresh_summary=True):
    if create_fresh_summary:
        s2_12_index_freqs_by_date = defaultdict(list)
        for tblname in tblnames:
            datestr = tblname.split('_')[1]
            (unused_src_imsis, unused_src2dest_imsis,
             s2_12_index_src_breakdown) = _get_imsi_dets(
                cur_local, tblname, src_region, dest_region,
                s2_12_to_region_name)
            for s2_12_index, n_imsis in s2_12_index_src_breakdown.items():
                s2_12_index_freqs_by_date[s2_12_index].append((datestr, n_imsis))
        with open(S2_12_FREQS_BY_DATE_JSON, 'w') as f:
            json.dump(s2_12_index_freqs_by_date, f)
    else:
        utils.warn("Not creating fresh summary data")
        with open(S2_12_FREQS_BY_DATE_JSON, 'r') as f:
            s2_12_index_freqs_by_date = json.load(f)
    results = []
    for s2_12_index, freqs_by_date in s2_12_index_freqs_by_date.items():
        s2_12_index = int(s2_12_index)
        grid12 = index2grid12.get(s2_12_index)
        print(grid12)
        print(freqs_by_date)
        prev_freq = None
        for datestr, freq in freqs_by_date:
            if prev_freq is not None:
                diff = freq - prev_freq
                diff_pct = (100*diff)/prev_freq
                if diff_pct > 50 and diff > 50:
                    diff_pct_str = "{:,}".format(round(diff_pct, 2))
                    results.append((s2_12_index, grid12, datestr, diff,
                        diff_pct_str))
            prev_freq = freq
    results.sort()
    for result in results:
        print(result)

def report_src_to_dest_region_shift(cur_local, src_region, dest_region,
        s2_12_to_region_name, date_str, make_fresh_shift_data=True):
    """
    E.g. 2016-04-06 has a high number of alleged Southlanders going to
    Northland. Surely some of them are actually Northlanders as evidenced by the
    number of Northland cell_id_rti's they connect to.
    """
    debug = True
    if make_fresh_shift_data:
        unused_con_rem, cur_rem = db.Pg.get_rem()
        datestr = date_str.replace('-', '')
        first_in_day_cat = "{}_{}".format(FIRST_IN_DAY_CAT_ROOT_KEY, datestr)
        sql_src_and_dest = """\
        SELECT imsi_rti AS
          imsi,
          s2_12_index,
          s2_12_home
        FROM {first_in_day_cat}
        ORDER BY imsi_rti
        """.format(first_in_day_cat=first_in_day_cat)
        cur_local.execute(sql_src_and_dest)
        imsis = []
        for row in cur_local.fetchall():
            src_index = row['s2_12_home']
            src_reg_derived = s2_12_to_region_name.get(src_index)
            dest_index = row['s2_12_index']
            dest_reg_derived = s2_12_to_region_name.get(dest_index)
            if src_reg_derived == src_region and dest_reg_derived == dest_region:
                imsis.append(int(row['imsi']))
        if debug: print(imsis)
        ## For each imsi, get all cell_id_rti's on day and grid12 - mostly src or
        ## dest?
        imsi_clause = db.Pg.nums2clause(imsis)
        sql_fakes = """\
        SELECT
            imsi_rti AS
          imsi,
          {connections}.start_date,
          cell_id_rti,
          grid12,
            label AS
          region_name
        FROM {connections}
        INNER JOIN
        {cell_info}
        USING(cell_id_rti)
        INNER JOIN
        {region_grid12}
        USING(grid12)
        INNER JOIN {region}
        USING(region_id)
        WHERE imsi_rti IN {imsi_clause}
        AND date = %s
        AND {cell_info}.start_date <= %s  -- using instead of date for possible performance benefits
        AND (
          {cell_info}.end_date IS NULL
          OR
          {cell_info}.end_date >= %s
        )
        ORDER BY imsi, {connections}.start_date
        """.format(connections=conf.CONNECTION_DATA, cell_info=conf.CELL_LOCATION,
            region_grid12=conf.REGION_GRID12, region=conf.REGION,
            imsi_clause=imsi_clause)
        cur_rem.execute(sql_fakes, (date_str, date_str, date_str))
        if debug: print(str(cur_rem.query, encoding='utf-8'))
        data = cur_rem.fetchall()
        if debug: print(data)
        with open(FAKE_TOURIST_CELLS_JSON, 'w') as f:
            json.dump(data, f)
    with open(FAKE_TOURIST_CELLS_JSON, 'r') as f:
        data = json.load(f)
    prev_imsi = None
    jumps = []
    region2letter = {src_region: 'S', dest_region: 'D'}
    imsi_jumps = []
    for (imsi, unused_start_date, unused_cell_id_rti, unused_grid12,
             region_name) in data:
        changed = (imsi != prev_imsi)
        if changed:
            if prev_imsi is not None:  ## close off last
                jumps.append((imsi, ''.join(imsi_jumps)))
            imsi_jumps = []
        else:
            imsi_jumps.append(region2letter.get(region_name, 'O'))
        prev_imsi = imsi
    jumps.append((imsi, ''.join(imsi_jumps)))
    if debug: pp(jumps)

def get_imsi_ranges(date_str, range_size=100):
    VERY_HIGH_IMSI = 10**25
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT DISTINCT imsi_rti
    FROM {connections}
    WHERE date = %s
    ORDER BY imsi_rti
    """.format(connections=conf.CONNECTION_DATA)
    cur_rem.execute(sql, (date_str, ))
    imsis = [row['imsi_rti'] for row in cur_rem.fetchall()]
    print(len(imsis))
    imsi_ranges = []
    finished = False
    for n in count():
        min_idx = n*range_size
        max_idx = (n+1)*range_size
        min_imsi = imsis[min_idx]
        try:
            max_imsi = imsis[max_idx]
        except IndexError:
            max_imsi = VERY_HIGH_IMSI
            finished = True
        imsi_ranges.append((min_imsi, max_imsi))
        if finished:
            break
    print(len(imsi_ranges))
    return imsi_ranges

def crazy_leaps(con_local, cur_local, date_str, region_ids=None,
        imsi_range_idxs=None, make_fresh_data=True):
    """
    When a crazy leap occurs we know that (at least) one of the cell_id_rti
    locations is incorrect.

    A crazy leap has to be distinguished from a valid shift. E.g. it would be
    valid for a car travelling between two rural towers to pass out of range of
    one up to 50km away in the backwards direction and switch connection to one
    50km ahead. That would look like 100km/s!

    Another case could be an air hostess travelling back and forth between
    Auckland and Hamilton. Every 20 minutes they would have travelled 130km i.e.
    390km/h. Or Auckland and Wellington (1000km in one hour i.e. 1000kph).

    So some shifts will be easy to identify as unambiguous crazy leaps. We can
    give those a red card. Others will not. In that case we might look at the
    IMSI as a whole (for a day) and see if the number of yellow cards is enough
    to clearly indicate crazy leaps somewhere in the day for that IMSI. We can
    look at the crazy leap combinations and see if there are any common
    denominators. These are the cell_id_rti's we will need to investigate for
    that day. Once we find those that are not where they claim to be on that
    date we can try to find out where they were instead.
    """
    debug = False
    if make_fresh_data:
        utils.warn("About to make fresh data - cancel if not meant to run this")
    report_red_flags = False
    red_carded_tblname = "{}_{}".format(conf.RED_CARDED,
        date_str.replace('-', ''))
    red_carded_pairs_tblname = "{}_{}".format(conf.RED_CARDED_PAIRS,
        date_str.replace('-', ''))
    #yellow_carded_tblname = "{}_{}".format(conf.YELLOW_CARDED,
    #    date_str.replace('-', ''))
    VERY_HIGH_KPH = 1000000
    
    imsi_range_size = 50000
    report_prog_every = 100000  ## records
    """
    Things can be super-fast if they happen quickly but some of that is because
    switching from one tower to another. So have to ignore distances of less
    than 100km (switching from one tower 75km in one direction to another 75km
    in the other direction).
    """
    min_dist = 150

    if region_ids:
        region_ids_clause = db.Pg.nums2clause(region_ids)
        sql = """\
        SELECT imsi_rti AS
          imsi,
            {connections}.start_date AS
          start_secs,
          start_date_str,
          cell_id_rti,
          tower_lat,
          tower_lon
        FROM {connections}
        INNER JOIN
        {cell_info}
        USING(cell_id_rti)
        INNER JOIN
        {region_grid12}
        USING(grid12)
        WHERE date = %s
        AND region_id IN {region_ids_clause}
        AND imsi_rti BETWEEN %s AND %s
        AND cell_id_rti NOT IN (0, 65535)  -- fake cell_id_rti's that connect to lots thus making false leaps
        AND {cell_info}.start_date <= %s  -- using instead of date for possible performance benefits
        AND (
          {cell_info}.end_date IS NULL
          OR
          {cell_info}.end_date >= %s
        )
        ORDER BY imsi, {connections}.start_date, cell_id_rti
        """.format(connections=conf.CONNECTION_DATA,
            cell_info=conf.CELL_LOCATION, region_ids_clause=region_ids_clause,
            region_grid12=conf.REGION_GRID12)
    else:
        sql = """\
        SELECT imsi_rti AS
          imsi,
            {connections}.start_date AS
          start_secs,
          start_date_str,
          cell_id_rti,
          tower_lat,
          tower_lon
        FROM {connections}
        INNER JOIN
        {cell_info}
        USING(cell_id_rti)
        WHERE date = %s
        AND imsi_rti BETWEEN %s AND %s
        AND cell_id_rti NOT IN (0, 65535)  -- fake cell_id_rti's that connect to lots thus making false leaps
        AND {cell_info}.start_date <= %s  -- using instead of date for possible performance benefits
        AND (
          {cell_info}.end_date IS NULL
          OR
          {cell_info}.end_date >= %s
        )
        ORDER BY imsi, {connections}.start_date, cell_id_rti
        """.format(connections=conf.CONNECTION_DATA,
            cell_info=conf.CELL_LOCATION)
    if make_fresh_data:
        unused_con_rem, cur_rem = db.Pg.get_rem()
        red_carded_rtis = defaultdict(int)
        red_carded_pairs = []
        #yellow_carded_imsi_dates = []
        imsi_ranges = get_imsi_ranges(date_str, range_size=imsi_range_size)
        n = 1
        if imsi_range_idxs:
            ranges2use = []
            for imsi_range_idx in imsi_range_idxs:
                try:
                    ranges2use.append(imsi_ranges[imsi_range_idx])
                except IndexError:
                    raise IndexError("Not able to take requested range of IMSIs"
                        " using index {} - only {} IMSI ranges available given "
                        "imsi_range_size of {}".format(imsi_range_idx,
                        len(imsi_ranges), imsi_range_size))
        else:
            ranges2use = imsi_ranges
        for min_imsi, max_imsi in ranges2use:
            print("Processing imsis from {} to {}".format(min_imsi, max_imsi))
            cur_rem.execute(sql, (date_str, min_imsi, max_imsi, date_str,
                date_str))
            if debug: print(str(cur_rem.query, encoding='utf-8'))
            prev_imsi = None
            prev_start_secs = None
            prev_coord = None
            prev_rti = None

            while True:
                row = cur_rem.fetchone()
                if not row:
                    break
                if debug: print(row)
                ## init
                imsi = row['imsi']
                start_secs = row['start_secs']
                start_date_str = row['start_date_str']
                lat = row['tower_lat']
                lon = row['tower_lon']
                rti = row['cell_id_rti']
                changed = (imsi != prev_imsi)
                current_coord = (lat, lon)
                #n_yellow_cards = 0
                if changed:
                    ## wrap up last round
                    #if n_yellow_cards > 1:
                    #    yellow_carded_imsi_dates.append((imsi, date_str))  ## specific to the imsi and date - seeing if there is something dodgy with some cell_id_rti's for that person on that date
                    ## reinit
                    #n_yellow_cards = 0
                    pass
                else: ## i.e continuing with the same IMSI, so do comparison!
                    if prev_coord == current_coord:
                        if debug:
                            print("Unchanged location so skipping unnecessary "
                                "calculations")
                    else:
                        ## get metrics
                        try:
                            loc_diff_kms = spatial.Pg.km_moved(cur_local, prev_coord,
                                current_coord, srid=conf.WGS84_SRID)
                        except Exception as e:
                            print("Hit error calculating distance. Orig error: "
                                "{}".format(e))
                            con_local.rollback()
                            continue
                        time_diff_mins = (start_secs - prev_start_secs)/60
                        time_diff_hours = time_diff_mins/60
                        try:
                            kph = loc_diff_kms/time_diff_hours
                        except ZeroDivisionError:
                            kph = VERY_HIGH_KPH
                        if debug:
                            print(round(loc_diff_kms, 1), round(time_diff_mins, 1),
                                round(kph, 1))
                        ## store details when instant red card
                        possible_fake_speed = (loc_diff_kms <= min_dist)
                        faster_than_plane = (kph > 900)
                        faster_than_car_but_is_not_plane = ((kph > 120)
                            and (time_diff_mins < 20))
                        seems_too_fast = (faster_than_plane
                            or faster_than_car_but_is_not_plane)
                        too_fast = (seems_too_fast and not possible_fake_speed)
                        if too_fast:
                            if report_red_flags:
                                print("Found red card for imsi {} starting {} "
                                "involving rti's {} and {} - "
                                "dist {:,}km mins {:,} (N red-carded RTI's "
                                "{:,})".format(
                                imsi, start_date_str,
                                rti, prev_rti,
                                round(loc_diff_kms, 1),
                                round(time_diff_mins, 1),
                                len(red_carded_rtis)))
                            else:
                                red_carded_rtis_n = len(red_carded_rtis)
                                if red_carded_rtis_n % 100 == 0:
                                    print("Red carded rti's N: {:,}".format(
                                        red_carded_rtis_n))  ## note - some red_card pairs add 1 or 0 new rti's but only bump up their counts
                            red_carded_pairs.append({
                                'rti': rti,
                                'prev_rti': prev_rti,
                                'imsi': imsi,
                                'start_date': start_date_str,
                                'kms': round(loc_diff_kms, 1),
                                'mins': round(time_diff_mins, 1),
                                })
                            ## Need both rti's because we can't tell which one is wrong, only that both can't be correct.
                            red_carded_rtis[rti] += 1
                            red_carded_rtis[prev_rti] += 1
                        ## add yellow_cards
                        #if kph > 120:
                        #    n_yellow_cards += 1
                ## wrap up for last one in last round
                #if n_yellow_cards > 1:
                #    yellow_carded_imsi_dates.append((imsi, date_str))
                ## reinit prevs for next round
                prev_imsi = imsi
                prev_start_secs = start_secs
                prev_coord = (lat, lon)
                prev_rti = rti
                if n % report_prog_every == 0:
                    print(utils.prog(n))
                n += 1
        if debug:
            pp(red_carded_rtis)
            #pp(yellow_carded_imsi_dates)

        ## Red carded
        db.Pg.drop_tbl(con_local, cur_local, tbl=red_carded_tblname)
        sql_make_red_tbl = """\
        CREATE TABLE {red_carded} (
            rti bigint,
            freq int
        )
        """.format(red_carded=red_carded_tblname)
        cur_local.execute(sql_make_red_tbl)
        con_local.commit()
        sql_insert_red_tpl = """\
        INSERT INTO {red_carded}
        (rti, freq)
        VALUES (%s, %s)
        """.format(red_carded=red_carded_tblname)
        for rti, freq in red_carded_rtis.items():
            cur_local.execute(sql_insert_red_tpl, (rti, freq))
        con_local.commit()
        
        ## Red carded pairs
        db.Pg.drop_tbl(con_local, cur_local, tbl=red_carded_pairs_tblname)
        sql_make_red_pairs_tbl = """\
        CREATE TABLE {red_carded_pairs} (
            rti bigint,
            prev_rti bigint,
            imsi bigint,
            start_date text,
            kms double precision,
            mins double precision
        )
        """.format(red_carded_pairs=red_carded_pairs_tblname)
        cur_local.execute(sql_make_red_pairs_tbl)
        con_local.commit()
        sql_insert_red_pairs_tpl = """\
        INSERT INTO {red_carded_pairs}
        (rti, prev_rti, imsi, start_date, kms, mins)
        VALUES (%s, %s, %s, %s, %s, %s)
        """.format(red_carded_pairs=red_carded_pairs_tblname)
        for d in red_carded_pairs:
            cur_local.execute(sql_insert_red_pairs_tpl, (d['rti'],
                d['prev_rti'], d['imsi'], d['start_date'], d['kms'], d['mins']))
        con_local.commit()
        

        ## Yellow carded
#         db.Pg.drop_tbl(con_local, cur_local, tbl=yellow_carded_tblname)
#         sql_make_yellow_tbl = """\
#         CREATE TABLE {yellow_carded} (
#             imsi bigint,
#             date_str text
#         )
#         """.format(yellow_carded=yellow_carded_tblname)
#         cur_local.execute(sql_make_yellow_tbl)
#         con_local.commit()
#         sql_insert_yellow_tpl = """\
#         INSERT INTO {yellow_carded}
#         (imsi, date_str)
#         VALUES (%s, %s)
#         """.format(yellow_carded=yellow_carded_tblname)
#         for imsi, date_str in yellow_carded_imsi_dates:
#             cur_local.execute(sql_insert_yellow_tpl, (imsi, date_str))
#         con_local.commit()

    sql_rti_dets = """\
    SELECT *
    FROM {red_carded}
    ORDER BY freq DESC
    """.format(red_carded=red_carded_tblname)
    try:
        cur_local.execute(sql_rti_dets)
    except db.pg.ProgrammingError as e:
        print("Error reading table {} - have you forgotten to set "
            "make_fresh_data=True?".format(red_carded_tblname))
        return
    data = cur_local.fetchall()
    pp(data[:15])

    sql_pair_dets = """\
    SELECT
      pairs.*,
        rti_freqs.freq AS
      rti_freq,
        prev_rti_freqs.freq AS
      prev_rti_freq
    FROM {red_carded_pairs} AS pairs
    INNER JOIN
    {red_carded} AS rti_freqs
    ON pairs.rti = rti_freqs.rti
    INNER JOIN
    {red_carded} AS prev_rti_freqs
    ON pairs.prev_rti = prev_rti_freqs.rti
    ORDER BY GREATEST(rti_freqs.freq, prev_rti_freqs.freq) DESC
    """.format(red_carded=red_carded_tblname,
        red_carded_pairs=red_carded_pairs_tblname)
    cur_local.execute(sql_pair_dets)
    data = cur_local.fetchall()
    for row in data[:1000]:
        print(row)

    print("""Try to identify a location in actual location using
    SELECT *
    FROM csm
    WHERE cell_id_rti = <rti here>
    ORDER BY cob_date;

    SELECT *
    FROM mod_p_locations
    WHERE cell_id_rti >= <rti here>
    ORDER BY cell_id_rti, effective_start;

    Then find in History using Ctrl-F <rti here>

    Then set task = 2 and look for location on date.

    The Question - is this cell_id_rti where the CSM 2 thinks it is?
    """)
    print("""Check red-carded events using a query like:
    SELECT
      start_date_str,
      cell_id_rti,
      duration,
      num_events
    FROM lbs_agg.fact_rti_summary
    WHERE date = '2015-11-30'
    AND imsi_rti = -8996249889876833453
    AND start_date_str BETWEEN '2015-11-30 16:29:30' AND '2015-11-30 16:32:36'
    ORDER BY start_date_str;
    """)

def crazy_leap_collector(con_local, cur_local, cur_local2, date_str,
        destroy_tbl=False):
    debug = False

    VERY_HIGH_KPH = 1000000
    CRAZY_TBL = 'crazy_leaps_collected'

    imsi_range_size = 50000
    report_events_prog_every = 100000
    report_leaps_prog_every = 100
    """
    Things can be super-fast if they happen quickly but some of that is because
    switching from one tower to another. So have to ignore distances of less
    than 100km (switching from one tower 75km in one direction to another 75km
    in the other direction).
    """
    min_dist = 150

    if destroy_tbl:
        sql_make_tbl = """\
        CREATE TABLE {crazy_tbl} (
            id bigserial,
            imsi bigint,
            prev_start_date_str text,
            start_date_str text,
            prev_rti bigint,
            rti bigint,
            prev_lat double precision,
            prev_lon double precision,
            lat double precision,
            lon double precision,
            loc_diff_kms double precision,
            time_diff_mins double precision,
            kph double precision,
            PRIMARY KEY(id)
        )
        """.format(crazy_tbl=CRAZY_TBL)
        cur_local.execute(sql_make_tbl)
        con_local.commit()
    sql = """\
    SELECT imsi_rti AS
      imsi,
        {connections}.start_date AS
      start_secs,
      start_date_str,
      cell_id_rti,
      tower_lat,
      tower_lon
    FROM {connections}
    INNER JOIN
    {cell_info}
    USING(cell_id_rti)
    WHERE date = %s
    AND imsi_rti BETWEEN %s AND %s
    AND cell_id_rti NOT IN (0, 65535)  -- fake cell_id_rti's that connect to lots thus making false leaps
    AND {cell_info}.start_date <= %s  -- using instead of date for possible performance benefits
    AND (
      {cell_info}.end_date IS NULL
      OR
      {cell_info}.end_date >= %s
    )
    ORDER BY imsi, {connections}.start_date, cell_id_rti
    """.format(connections=conf.CONNECTION_DATA,
        cell_info=conf.CELL_LOCATION)
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql_insert = """\
    INSERT INTO {crazy_tbl}
    (imsi, prev_start_date_str, start_date_str, prev_rti, rti,
     prev_lat, prev_lon, lat, lon, loc_diff_kms, time_diff_mins, kph)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """.format(crazy_tbl=CRAZY_TBL)
    imsi_ranges = get_imsi_ranges(date_str, range_size=imsi_range_size)
    n_events = 0
    n_leaps = 0
    for min_imsi, max_imsi in imsi_ranges:
        print("Processing imsis from {} to {}".format(min_imsi, max_imsi))
        cur_rem.execute(sql, (date_str, min_imsi, max_imsi, date_str,
            date_str))
        if debug: print(str(cur_rem.query, encoding='utf-8'))
        prev_imsi = None
        prev_start_date_str = None
        prev_start_secs = None
        prev_coord = None
        prev_rti = None
        while True:
            row = cur_rem.fetchone()
            if not row:
                break
            n_events += 1
            if debug: print(row)
            ## init
            imsi = row['imsi']
            start_secs = row['start_secs']
            start_date_str = row['start_date_str']
            lat = row['tower_lat']
            lon = row['tower_lon']
            rti = row['cell_id_rti']
            changed = (imsi != prev_imsi)
            current_coord = (lat, lon)
            if not changed:  ## i.e continuing with the same IMSI, so do comparison!
                if prev_coord == current_coord:
                    if debug:
                        print("Unchanged location so skipping unnecessary "
                            "calculations")
                else:
                    try:
                        loc_diff_kms = spatial.Pg.km_moved(cur_local,
                            prev_coord, current_coord, srid=conf.WGS84_SRID)
                    except Exception as e:
                        print("Hit error calculating distance. Orig error: "
                            "{}".format(e))
                        con_local.rollback()
                        continue
                    time_diff_mins = (start_secs - prev_start_secs)/60
                    time_diff_hours = time_diff_mins/60
                    try:
                        kph = loc_diff_kms/time_diff_hours
                    except ZeroDivisionError:
                        kph = VERY_HIGH_KPH
                    if debug:
                        print(round(loc_diff_kms, 1), round(time_diff_mins, 1),
                            round(kph, 1))
                    possible_fake_speed = (loc_diff_kms <= min_dist)
                    faster_than_plane = (kph > 900)
                    faster_than_car_but_is_not_plane = ((kph > 120)
                        and (time_diff_mins < 20))
                    seems_too_fast = (faster_than_plane
                        or faster_than_car_but_is_not_plane)
                    too_fast = (seems_too_fast and not possible_fake_speed)
                    if too_fast:
                        n_leaps += 1
                        if n_leaps % report_leaps_prog_every == 0:
                            print(utils.prog(n_leaps, label='crazy leaps'))
                        prev_lat, prev_lon = prev_coord
                        rec_data = (imsi, prev_start_date_str, start_date_str,
                            prev_rti, rti, prev_lat, prev_lon, lat, lon,
                            round(loc_diff_kms, 1), round(time_diff_mins, 1),
                            round(kph, 1))
                        cur_local2.execute(sql_insert, rec_data)
                        con_local.commit()  ## bound to crash so don't want to lose the data already obtained
            prev_imsi = imsi
            prev_start_date_str = start_date_str
            prev_start_secs = start_secs
            prev_coord = (lat, lon)
            prev_rti = rti
            if n_events % report_events_prog_every == 0:
                print(utils.prog(n_events, label='events'))

def main():
    con_local, cur_local, cur_local2 = db.Pg.get_local()
    date_str = '2017-08-01'
    crazy_leap_collector(con_local, cur_local, cur_local2, date_str,
        destroy_tbl=False)
    
    '''
    GET_DATA = 1
    CHECK_SUSPECT_RTI = 2

    task = 2

    if task == GET_DATA:
        imsi_range_idxs = (0, 5, 10, 20)  ## None if unlimited. OK to do "sample" run - seems to be enough to sift things out.
        crazy_leaps(con_local, cur_local, date_str='2017-04-17',
            region_ids=None, #[conf.TARANAKI_RTO_ID, conf.HAWKES_BAY_RTO_ID],
            imsi_range_idxs=imsi_range_idxs, make_fresh_data=False)

    elif task == CHECK_SUSPECT_RTI:
        rti = 45932
        rtis_to_exclude = [45931, 45933, 45934, 45935, 45936]
        (start_datetime_str,
         end_datetime_str) = dates.Dates.date_str_to_datetime_str_range(
             '2017-04-17', halfday=True)
        findrels.FindRelocations.get_likely_location_dts(cur_local,
            start_datetime_str, end_datetime_str, rti, rtis_to_exclude)
    '''

if __name__ == '__main__':
    main()
