#! /usr/bin/env python3

"""
Going to focus on Northland to Wairarapa visits to keep numbers small.
"""
from collections import Counter
import datetime
from pprint import pprint as pp
from random import sample

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, dates, db, flow, utils

"""
Note - regional boundaries are not always in alignment - focus here on ones
where the boundaries in https://en.wikipedia.org/wiki/Regions_of_New_Zealand
align with our RTO boundaries.

Populations 15+ 2016 - http://nzdotstat.stats.govt.nz/wbos/Index.aspx?DataSetCode=TABLECODE7501#

Market shares - https://drive.google.com/a/telecomdigital.co.nz/file/d/...
  ...0Bwi8NvgViAeMMFlaMjhTQVpyMlU/view?usp=sharing Sheet 3.
via John. From Camila.

Stale data is as at the END_TUES_DATE.

International tourist numbers come from
http://www.stats.govt.nz/browse_for_stats/population/Migration/...
  ...international-visitor-arrivals-aug-17.aspx
"""
END_TUES_DATE = '2017-09-05'  ## far enopugh back so some room for IMSIs to do something afterwards

STATS_NZ_INT_TOURISTS = {2016: 3358580, }

NORTHLAND = {'lbl': 'Northland', 'region_id': conf.NORTHLAND_RTO_ID,
    'popn15+': 135000, 'market_share': 0.25, 'stale_home_loc': 49661}
AUCKLAND = {'lbl': 'Auckland', 'region_id': conf.AUCK_RTO_ID,
    'popn15+': 1296200, 'market_share': 0.26, 'stale_home_loc': 469817}
NELSON_TASMAN = {'lbl': 'Nelson-Tasman', 'region_id': conf.NELSON_RTO_ID,
    'popn15+': 82300, 'market_share': 0.42, 'stale_home_loc': 51093}
SOUTHLAND = {'lbl': 'Southland', 'region_id': conf.SOUTHLAND_RTO_ID,
    'popn15+': 78300, 'market_share': 0.58, 'stale_home_loc': 59370}
MARLBOROUGH = {'lbl': 'Marlborough', 'region_id': conf.MARLBOROUGH_RTO_ID,
    'popn15+': 37400, 'market_share': 0.44, 'stale_home_loc': 24442}

REGIONS = [NORTHLAND, AUCKLAND, NELSON_TASMAN, SOUTHLAND, MARLBOROUGH]
#REGIONS = [NELSON_TASMAN, ]

sql_grid2rto_tpl = """
SELECT grid_id, region.label AS rto_name
FROM lbs_master.region_grid AS region_grid
INNER JOIN lbs_master.region_to_group AS region_to_group
ON region_grid.region_id = region_to_group.region_id
INNER JOIN lbs_master.region AS region
ON region_grid.region_id = region.region_id
WHERE region_group_id = {rto_region_group_id}
"""
sql_commuters_tpl = """\
SELECT
  imsi_rti,
  end_tues_date,
    commuters.grid12 AS
  dest_grid12,
    rto_name AS
  dest_rto
FROM lbs_etl_test.commuters AS commuters
INNER JOIN ({sql_grid2rto}) AS grid2rto
ON commuters.grid12 = grid2rto.grid_id
WHERE end_tues_date = '{date_str_tues}'
AND days_seen >= 3
"""
sql_origins_tpl = """\
SELECT
  imsi_rti,
  end_tues_date,
  home_grid12,
    rto_name AS
  home_rto
FROM lbs_etl_test.grid12_home_location AS home
INNER JOIN ({sql_grid2rto}) AS grid2rto
ON home.home_grid12 = grid2rto.grid_id
WHERE end_tues_date = '{date_str_tues}'
AND origin_type = 'domestic'
"""

sql_grid2rto = """
SELECT grid_id, region.label AS rto_name
FROM lbs_master.region_grid AS region_grid
INNER JOIN lbs_master.region_to_group AS region_to_group
ON region_grid.region_id = region_to_group.region_id
INNER JOIN lbs_master.region AS region
ON region_grid.region_id = region.region_id
WHERE region_group_id = {rto_region_group_id}
""".format(rto_region_group_id=conf.RTO_REGION_GROUP_ID)

def international_total_check(year=2016, use_stale=False, show_prog=True):
    """
    For a given year we allege a certain number of international IMSIs
    (including local SIMs) appeared on our network for the first time. How does
    that compare with arrivals for international tourists?

    Note - even though Spark only gets a share of the total international
    tourists it may still touch nearly every single roaming IMSI because roaming
    uses non-preferred carriers if the preferred carrier is unavailable or not
    optimal.
    """
    if use_stale:
        product_int_year = 4079014
        print("\n\nNot recalculating fresh data for internationals in year - "
            "using value for 2016\n\n")
    else:
        unused, cur_hive = db.Hive.hive()
        sql = """\
        SELECT COUNT(DISTINCT imsi_rti) AS n_international
        FROM lbs_etl_test.grid12_home_location AS home
        WHERE home.origin_type = 'international'
        AND SUBSTR(end_tues_date, 1, 4) = '2016'
        """
        cur_hive.execute(sql, async=True)
        if show_prog: db.Hive.fetch_prog(cur_hive)
        product_int_year = cur_hive.fetchone()[0]
    print("\nInternational tourists {year}\N{SUPERSCRIPT ONE}:"
        " {stats_nz_int_tourists:,}"
        "\nAlleged international IMSIs "
        "(Spark only\N{SUPERSCRIPT TWO}): {product_int_year:,}"
        "\n\n\N{SUPERSCRIPT ONE}. Could be an underestimate."
        "\n\N{SUPERSCRIPT TWO}. For the roaming portion of international "
        "tourists Spark (and other carriers) may touch all tourists because "
        "their phones use other carriers when the preferred one is not "
        "available/optimal. So the Spark numbers could also be the total "
        "numbers. "
        .format(year=year, stats_nz_int_tourists=STATS_NZ_INT_TOURISTS[year],
            product_int_year=product_int_year))
    return product_int_year

def international_local_sim_tot_check(year, product_int_year, use_stale=False,
        show_prog=True):
    """
    Look at all local SIM international imsis that started in year. Each week
    (common end_tues_date) only contains the IMSIs that were identified as such
    in that week. It's the diff.
    """
    if use_stale:
        n_int_local_sim = 532802
        print("\n\nNot recalculating fresh data for international local sims in"
            " year - using value for 2016\n\n")
    else:
        unused, cur_hive = db.Hive.hive()
        sql = """\
        SELECT COUNT(DISTINCT imsi_rti) AS n_int_local_sim
        FROM lbs_etl_test.international_local_sim
        WHERE SUBSTR(end_tues_date, 1, 4) = '{year}'
        """.format(year=year)
        cur_hive.execute(sql, async=True)
        if show_prog: db.Hive.fetch_prog(cur_hive)
        n_int_local_sim = cur_hive.fetchone()[0]
    print("\n\n{n_int_local_sim:,} local SIM internationals out of "
        "{product_int_year:,} total in {year} (NZ Stats comparison is "
        "{stats_nz_int_tourists:,})\n".format(
        n_int_local_sim=n_int_local_sim, product_int_year=product_int_year,
        year=year, stats_nz_int_tourists=STATS_NZ_INT_TOURISTS[year]))

def international_local_sim_explore(use_stale=False, show_prog=True):
    debug = True
    min_date_str = (datetime.datetime.strptime(END_TUES_DATE, '%Y-%m-%d')
        - datetime.timedelta(days=10)).strftime('%Y-%m-%d')
    max_date_str = (datetime.datetime.strptime(END_TUES_DATE, '%Y-%m-%d')
        - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    if use_stale:
        # -7569066860622785728, -3220630383077876523, -6273677767908041577, 3272261016560721521, 4697673133214097455, 5295484435278594404, -5692282370111902316, 3808453325896747452, -1828865049207154468,  5683857991316993469
        imsis2check = [2534792989538230676, 4797053884567515969,
        -3964536308793323800, -8577583766316154967, -4780440187895260370,
        2512953597895824818, -3456320819424655988, 3131592556887694479,
        6028990921975454361, -1395599230163042419]
    else:
        if debug: print(min_date_str, END_TUES_DATE)
        unused, cur_hive = db.Hive.hive()
        sql = """\
        SELECT imsi_rti
        FROM lbs_etl_test.international_local_sim
        WHERE end_tues_date = '{end_tues_date}'
        ORDER BY imsi_rti
        """.format(end_tues_date=END_TUES_DATE)
        cur_hive.execute(sql)
        if show_prog: db.Hive.fetch_prog(cur_hive)
        new_imsis = [row[0] for row in cur_hive.fetchall()]
        imsis2check = sample(new_imsis, 10)
        if debug: print(imsis2check)
    for imsi in imsis2check:
        utils.report_on_imsi(imsi, min_date_str, max_date_str)
        input("Next?")

def international_local_sim_flow_videos():
    confs = [
        #(-3220630383077876523, '2017-09-28', '2017-10-09'),
        (-7990655199013925685, '2017-09-02', '2017-10-09'),
    ]
    for conf in confs:
        utils.report_on_imsi(*conf)
        flow.FlowMap.make_imsi_travel_video(*conf)
        input('Next ...')

def itinerants_identified():
    """
    Which IMSI's have lots of different home regions? How common is it? What are
    some of the extreme cases doing? Missing some Local SIM internationals? Note
    -- not looking at back and forth just the total regions that were ever home.

    (1, 3745004),
    (2, 828374),
    (3, 215061),
    (4, 69784),
    (5, 28325),
    (6, 13653),
    (7, 7029),
    (8, 3643),
    (9, 1999),
    (10, 1056),
    (11, 596),
    (12, 299),
    (13, 172),
    (14, 96),
    (15, 50),
    (16, 22),
    (17, 14),
    (18, 5),
    (19, 3),
    (20, 1),
    (21, 2),
    (23, 1)

    i.e. 4,788,439 living in 1-3 regions only i.e. 97.5% of 4,915,189 total
    domestic IMSIs.

    Some extreme cases to examine: [-1852062520891644606, 2153395755593234888,
    2407937436528533967, 822354902981307450, -6444264714225609113,
    -2376089747796375165, -322482752286428800, -8067725046861724202,
    -1969776946490116676, 1081363014045856596]
    """
    unused, cur_hive = db.Hive.hive()
    sql = """\
    SELECT imsi_rti, COUNT(*) AS n_home_region
    FROM (
      SELECT DISTINCT
        imsi_rti,
        region_grid.region_id
      FROM lbs_etl_test.grid12_home_location AS home
      INNER JOIN lbs_master.region_grid AS region_grid
      ON (home.home_grid12 = region_grid.grid_id)
      INNER JOIN lbs_master.region_to_group AS region2group
      ON (region_grid.region_id = region2group.region_id)
      WHERE home.origin_type = 'domestic'
      AND region2group.region_group_id = {rto_region_group_id}
    ) AS src
    GROUP BY imsi_rti
    ORDER BY n_home_region DESC, imsi_rti
    """.format(rto_region_group_id=conf.RTO_REGION_GROUP_ID)
    cur_hive.execute(sql)
    db.Hive.fetch_prog(cur_hive)
    data = cur_hive.fetchall()
    region_counts = Counter([freq for unused_imsi, freq in data])
    pp(region_counts.items())
    first_imsis = [imsi for imsi, unused_freq in data[:10]]
    print(first_imsis)

def indiv_itinerant_report(imsi):
    """
    Actually a Local SIM international we didn't pick up? Long-term
    tourist/worker?

    [(-1852062520891644606, '2015-01-13', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2015-01-20', 'Nelson Tasman RTO', 57270120),
    (-1852062520891644606, '2015-01-27', 'Dunedin RTO', 88164970),
    (-1852062520891644606, '2015-02-03', 'Canterbury RTO', 57250014),
    (-1852062520891644606, '2015-02-10', 'Canterbury RTO', 57250014),
    (-1852062520891644606, '2015-02-17', 'Canterbury RTO', 57250014),
    (-1852062520891644606, '2015-02-24', 'Canterbury RTO', 57250014),
    (-1852062520891644606, '2015-03-03', 'Canterbury RTO', 57250014),
    (-1852062520891644606, '2015-03-10', 'Clutha RTO', 88171292),
    (-1852062520891644606, '2015-03-17', 'Fiordland RTO', 89038960),
    (-1852062520891644606, '2015-03-24', 'Fiordland RTO', 89038960),
    (-1852062520891644606, '2015-03-31', 'Southland RTO', 89036324),
    (-1852062520891644606, '2015-04-07', 'Fiordland RTO', 89038960),
    (-1852062520891644606, '2015-04-14', 'Fiordland RTO', 89038960),
    (-1852062520891644606, '2015-04-21', 'Canterbury RTO', 57250014),
    (-1852062520891644606, '2015-04-28', 'Canterbury RTO', 57250014),
    (-1852062520891644606, '2015-05-05', 'Canterbury RTO', 57250014),
    (-1852062520891644606, '2015-05-12', 'Canterbury RTO', 57250014),
    (-1852062520891644606, '2015-05-19', 'West Coast RTO', 57244027),
    (-1852062520891644606, '2015-07-07', 'Coromandel RTO', 57371674),
    (-1852062520891644606, '2015-07-14', 'Coromandel RTO', 57371674),
    (-1852062520891644606, '2015-07-21', 'Coromandel RTO', 57371674),
    (-1852062520891644606, '2015-07-28', 'Coromandel RTO', 57371674),
    (-1852062520891644606, '2015-08-04', 'Coromandel RTO', 57371674),
    (-1852062520891644606, '2015-09-29', 'Taranaki RTO', 57191842),
    (-1852062520891644606, '2015-10-06', 'Taranaki RTO', 57191842),
    (-1852062520891644606, '2015-10-13', 'Taranaki RTO', 57191842),
    (-1852062520891644606, '2015-10-20', 'Waikato RTO', 57371394),
    (-1852062520891644606, '2015-10-27', 'Auckland RTO', 57382250),
    (-1852062520891644606, '2015-11-03', 'Auckland RTO', 57382250),
    (-1852062520891644606, '2015-11-10', 'Auckland RTO', 57382250),
    (-1852062520891644606, '2015-11-17', 'Auckland RTO', 57382250),
    (-1852062520891644606, '2015-11-24', 'Auckland RTO', 57382250),
    (-1852062520891644606, '2015-12-01', 'Northland RTO', 57170854),
    (-1852062520891644606, '2015-12-08', 'Northland RTO', 57170854),
    (-1852062520891644606, '2015-12-15', 'Northland RTO', 57170854),
    (-1852062520891644606, '2015-12-22', 'Northland RTO', 57172041),
    (-1852062520891644606, '2015-12-29', 'Northland RTO', 57172041),
    (-1852062520891644606, '2016-01-05', 'Northland RTO', 57172041),
    (-1852062520891644606, '2016-03-01', 'Rotorua RTO', 57373976),
    (-1852062520891644606, '2016-03-08', 'Auckland RTO', 57173740),
    (-1852062520891644606, '2016-03-15', 'Auckland RTO', 57173740),
    (-1852062520891644606, '2016-03-22', 'Auckland RTO', 57173740),
    (-1852062520891644606, '2016-03-29', 'Auckland RTO', 57173740),
    (-1852062520891644606, '2016-04-05', 'Auckland RTO', 57173740),
    (-1852062520891644606, '2016-04-12', 'Coromandel RTO', 57381740),
    (-1852062520891644606, '2016-04-19', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-04-26', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-05-03', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2016-05-10', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-05-17', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-05-24', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-05-31', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-06-07', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-06-14', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-06-21', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-06-28', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-07-05', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-07-12', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-07-19', 'Bay of Plenty RTO', 57373639),
    (-1852062520891644606, '2016-07-26', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2016-08-02', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2016-08-09', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-08-16', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2016-08-23', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2016-08-30', 'Manawatu RTO', 57279081),
    (-1852062520891644606, '2016-09-06', 'Manawatu RTO', 57279081),
    (-1852062520891644606, '2016-09-13', 'Manawatu RTO', 57279081),
    (-1852062520891644606, '2016-09-20', 'Wellington RTO', 57279842),
    (-1852062520891644606, '2016-09-27', 'Wellington RTO', 57279842),
    (-1852062520891644606, '2016-10-04', 'Wellington RTO', 57279842),
    (-1852062520891644606, '2016-10-11', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2016-10-25', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-11-01', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-11-08', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-11-15', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-11-22', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-11-29', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-12-06', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-12-13', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-12-20', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2016-12-27', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2017-01-03', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2017-01-10', 'Taranaki RTO', 57190476),
    (-1852062520891644606, '2017-01-17', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2017-01-24', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2017-02-07', 'Marlborough RTO', 57264854),
    (-1852062520891644606, '2017-02-14', 'Nelson Tasman RTO', 57269524),
    (-1852062520891644606, '2017-02-21', 'Nelson Tasman RTO', 57269524),
    (-1852062520891644606, '2017-02-28', 'West Coast RTO', 57223423),
    (-1852062520891644606, '2017-03-07', 'Nelson Tasman RTO', 57269408),
    (-1852062520891644606, '2017-03-14', 'Canterbury RTO', 57248836),
    (-1852062520891644606, '2017-03-21', 'Canterbury RTO', 57248836),
    (-1852062520891644606, '2017-03-28', 'Canterbury RTO', 57248836),
    (-1852062520891644606, '2017-04-04', 'Canterbury RTO', 57248836),
    (-1852062520891644606, '2017-04-11', 'Lake Wanaka RTO', 89041436),
    (-1852062520891644606, '2017-04-18', 'Lake Wanaka RTO', 89041436),
    (-1852062520891644606, '2017-04-25', 'Central Otago RTO', 88167020),
    (-1852062520891644606, '2017-05-02', 'Central Otago RTO', 88167020),
    (-1852062520891644606, '2017-05-09', 'Central Otago RTO', 88167020),
    (-1852062520891644606, '2017-05-16', 'Central Otago RTO', 88167020),
    (-1852062520891644606, '2017-05-23', 'Waitaki RTO', 88165334),
    (-1852062520891644606, '2017-05-30', 'Nelson Tasman RTO', 57269302),
    (-1852062520891644606, '2017-06-06', 'Nelson Tasman RTO', 57269302),
    (-1852062520891644606, '2017-06-13', 'Marlborough RTO', 57264635),
    (-1852062520891644606, '2017-06-20', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2017-06-27', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2017-07-04', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2017-07-11', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2017-07-18', 'Kapiti-Horowhenua RTO', 57280421),
    (-1852062520891644606, '2017-07-25', 'Kawerau-Whakatane RTO', 57374647),
    (-1852062520891644606, '2017-08-01', 'Kawerau-Whakatane RTO', 57374647),
    (-1852062520891644606, '2017-08-08', 'Kawerau-Whakatane RTO', 57374647),
    (-1852062520891644606, '2017-08-15', 'Lake Taupo RTO', 57368448)]
    """
    unused, cur_hive = db.Hive.hive()
    sql = """\
    SELECT imsi_rti, end_tues_date, region.label, home_grid12
    FROM lbs_etl_test.grid12_home_location AS home
    INNER JOIN lbs_master.region_grid AS region_grid
    ON (home.home_grid12 = region_grid.grid_id)
    INNER JOIN lbs_master.region AS region
    ON (region_grid.region_id = region.region_id)
    INNER JOIN lbs_master.region_to_group AS region2group
    ON (region_grid.region_id = region2group.region_id)
    WHERE imsi_rti = {imsi}
    AND region2group.region_group_id = {rto_region_group_id}
    ORDER BY end_tues_date
    """.format(imsi=imsi,
        rto_region_group_id=conf.RTO_REGION_GROUP_ID)
    cur_hive.execute(sql)
    db.Hive.fetch_prog(cur_hive)
    data = cur_hive.fetchall()
    pp(data)

def home_location_popn_check(use_stale=False, show_prog=True):
    """
    We are sanity checking numbers, not trying to get precise numbers.

    The number of people 15+ (as at 2016) is being treated as an approximate
    upper limit for the number of IMSIs in a region. Although some people will
    have multiple devices it is likely this is outweighed by the number of
    people without mobiles.

    If Spark gets its market share of IMSIs in a region we would expect to get
    no more than market_share*max_imsis_in_region.
    """
    for region in REGIONS:
        if use_stale:
            home_loc = region['stale_home_loc']
        else:
            unused, cur_hive = db.Hive.hive()
            sql = """\
            SELECT COUNT(DISTINCT imsi_rti) AS n_popn
            FROM lbs_etl_test.grid12_home_location AS home
            INNER JOIN lbs_master.region_grid AS region_grid
            ON home.home_grid12 = region_grid.grid_id
            WHERE end_tues_date = '{tues_end_date}'
            AND region_id = {region_id}
            AND home.origin_type = 'domestic'
            """.format(tues_end_date='2017-10-03',
                region_id=region['region_id'])
            cur_hive.execute(sql, async=True)
            if show_prog: db.Hive.fetch_prog(cur_hive)
            home_loc = cur_hive.fetchone()[0]
        expected_spark_imsis = int(
            round(region['popn15+']*region['market_share'], -3))
        print("\n* {} *****"
            "\nApprox max IMSIs in region\N{SUPERSCRIPT ONE}: {:,}"
            "\nSpark market share: {:,}"
            "\nApprox max Spark IMSIs in region: {:,}"
            "\nAlleged Spark IMSIs in region\N{SUPERSCRIPT TWO}: {:,}"
            .format(region['lbl'], region['popn15+'], region['market_share'],
            expected_spark_imsis, home_loc))
    print("\n\N{SUPERSCRIPT ONE}. The number of people 15+ (as at 2016) is "
        "being treated as an approximate upper limit for the number of IMSIs in"
        " a region. Although some people will have multiple devices it is "
        "likely this is outweighed by the number of people without mobiles."
        "\n\N{SUPERSCRIPT TWO}. From lbs_etl_test.grid12_home_location with data"
        " filtered to only include domestic IMSIs.")

def _get_where(extremes):
    if not extremes:
        where_clause = "dest_rto <> home_rto"
    else:
        where_clause = (' OR '.join(["(dest_rto = '{}' AND home_rto = '{}')"
            .format(pair_dic['dest'], pair_dic['home'])
            for pair_dic in extremes]))
    return where_clause

def frequent_region_back_n_forth(date_str_tues, extremes=None):
    debug = True
    verbose = False
    min_date_str, max_date_str = dates.Dates.end_tues_date_to_range(
        date_str=date_str_tues)
    unused, cur_hive = db.Hive.hive()
    sql_commuters = sql_commuters_tpl.format(sql_grid2rto=sql_grid2rto,
        date_str_tues=date_str_tues)
    sql_origins = sql_origins_tpl.format(sql_grid2rto=sql_grid2rto,
        date_str_tues=date_str_tues)
    sql_tpl = """\
    SELECT DISTINCT
      qry_commuters.imsi_rti,
      dest_rto,
      home_rto
    FROM ({sql_commuters}) AS qry_commuters
    INNER JOIN
    ({sql_origins}) AS qry_origins
    ON qry_commuters.imsi_rti = qry_origins.imsi_rti
    WHERE {{where_clause}}
    ORDER BY imsi_rti, dest_rto, home_rto
    """.format(sql_commuters=sql_commuters, sql_origins=sql_origins)
    where_clause = _get_where(extremes)
    sql = sql_tpl.format(where_clause=where_clause)
    if debug and verbose: print(sql)
    cur_hive.execute(sql)
    data = cur_hive.fetchall()
    if debug:
        for n, row in enumerate(data, 1):
            print(row)
            if n > 500:
                print("Displaying 500 records out of {:,}".format(len(data)))
                break
    if len(data) > 20:
        data2check = sample(data, 10)
    else:
        data2check = data
    if debug: print(data2check)
    for imsi, dest_rto, home_rto in data2check:
        ## home grid12
        sql_home_grid12 = """\
        SELECT home_grid12
        FROM lbs_etl_test.grid12_home_location
        WHERE end_tues_date = '{date_str_tues}'
        AND imsi_rti = {imsi}
        """.format(date_str_tues=date_str_tues, imsi=imsi)
        cur_hive.execute(sql_home_grid12)
        home_grid12 = cur_hive.fetchone()[0]
        msg = ("Confirming {} 'commuted' (hopped back and forth) between home "
            "region {} and dest region {} (home_grid12 = {})"
            .format(imsi, home_rto, dest_rto, home_grid12))
        utils.report_on_imsi(imsi, min_date_str, max_date_str, msg,
            inc_rti=True, inc_grid12=True)
        resp = input("e to exit otherwise continue")
        if resp.lower() == 'e':
            break

def frequent_region_back_n_forth_sanity(date_str_tues, extremes_only=False):
    """
    Look at sample of people considered commuters for a given week
    (end_tues_date?). Look OK?

    Commuters travel from one region to another at least 3 times in a week.

    Note - the delay in adding regions to grids till the end stages of the data
    pipeline does theoretically understate commuting in edge cases. Imagine a
    person living in Region A seen in grid 1 twice (in Region B) and in grid 2
    once (also in Region B). Because neither grid was seen three times in the
    week the person is not considered a commuter from Region A to Region B even
    though they have in fact commuted there three times in a week.

    Also included will be people who live near the boundaries of regions and
    whose device connects promiscuously to towers on both sides.

    Southland RTO              2017-10-04 15:02:52 -46.3855876602104    168.349217715251     Waikiwi 3                 42433
    Fiordland RTO              2017-10-04 15:03:02 -45.7648978919216    168.297669459996     White Hill 2              52415
    Southland RTO              2017-10-04 15:03:06 -46.3463875164738    168.342534752693     Lorneville 1              43264
    Fiordland RTO              2017-10-04 15:03:07 -45.7648978919216    168.297669459996     White Hill 2              52415

    About 70km apart.

    Waikato RTO                2017-10-03 10:56:32 -37.5373021372267    175.469619489722     Tahuna 2                  27832
    Waikato RTO                2017-10-03 10:56:37 -37.6869573460075    175.608131238174     Maungatapu 3             465285
    Waikato RTO                2017-10-03 10:56:39 -37.6869573460075    175.608131238174     Maungatapu 3              27773
    Waikato RTO                2017-10-03 10:56:40 -37.6869573460075    175.608131238174     Maungatapu 3             465285
    Waikato RTO                2017-10-03 10:56:41 -37.6869573460075    175.608131238174     Maungatapu 3              27773
    Waikato RTO                2017-10-03 10:56:49 -37.6869573460075    175.608131238174     Maungatapu 3             465285
    Waikato RTO                2017-10-03 11:01:49 -37.5373021372267    175.469619489722     Tahuna 2                 313465
    Coromandel RTO             2017-10-03 11:11:32 -37.3786291560377    175.693968668237     Paeroa 2                  27662
    Waikato RTO                2017-10-03 11:11:33 -37.5373021372267    175.469619489722     Tahuna 2                 313465
    Coromandel RTO             2017-10-03 11:11:34 -37.3786291560377    175.693968668237     Paeroa 2                  27662
    Waikato RTO                2017-10-03 11:11:36 -37.5373021372267    175.469619489722     Tahuna 2                 313465
    Coromandel RTO             2017-10-03 11:14:14 -37.3786291560377    175.693968668237     Paeroa 2                 301947
    Waikato RTO                2017-10-03 11:14:14 -37.5373021372267    175.469619489722     Tahuna 1                  27831
    Waikato RTO                2017-10-03 11:14:15 -37.5373021372267    175.469619489722     Tahuna 1                  27831
    Coromandel RTO             2017-10-03 11:14:15 -37.3786291560377    175.693968668237     Paeroa 2                 301947

    About 33km apart.

    Auckland RTO               2017-10-03 09:49:51 -37.035972230778     174.900373503206     Manurewa South 1         481391
    Waikato RTO                2017-10-03 09:49:52 -37.194957613584     175.019358284593     Puketutu 1               309615
    Auckland RTO               2017-10-03 09:51:01 -37.0472748618663    174.926882695168     NZ Dairy Foods (Takanini) 1     482159

    About 25km apart.
    """
    if not extremes_only:
        frequent_region_back_n_forth(date_str_tues)
    extremes = [
        {'dest': 'Auckland RTO', 'home': 'Dunedin RTO'},
        {'dest': 'Coromandel RTO', 'home': 'Southland RTO'},
    ]
    frequent_region_back_n_forth(date_str_tues, extremes)

def check_alleged_commuter(imsi, date_str_tues, extremes):
    """
    The general analysis of commuters looks at all distinct IMSIs. Here we want
    to look at the underlying data for a specific IMSI and check that the IMSI
    rows being kept are in fact in different regions as well as being in
    different grid squares and having days_seen >= 3.
    """
    debug = False
    verbose = False
    unused, cur_hive = db.Hive.hive()
    sql_commuters = sql_commuters_tpl.format(sql_grid2rto=sql_grid2rto,
        date_str_tues=date_str_tues)
    sql_origins = sql_origins_tpl.format(sql_grid2rto=sql_grid2rto,
        date_str_tues=date_str_tues)
    sql_tpl = """\
    SELECT -- NOT distinct unlike general analysis of all IMSIs - we want to check filtering correct
      qry_commuters.imsi_rti,
      dest_grid12,
      home_grid12,
      dest_rto,
      home_rto
    FROM ({sql_commuters}) AS qry_commuters
    INNER JOIN
    ({sql_origins}) AS qry_origins
    ON qry_commuters.imsi_rti = qry_origins.imsi_rti
    WHERE {{where_clause}}
    AND qry_commuters.imsi_rti = {imsi}
    ORDER BY imsi_rti, dest_rto, home_rto
    """.format(sql_commuters=sql_commuters, sql_origins=sql_origins,
        imsi=imsi)
    where_clause = _get_where(extremes)
    sql = sql_tpl.format(where_clause=where_clause)
    if debug and verbose: print(sql)
    cur_hive.execute(sql)
    data = cur_hive.fetchall()
    pp(data)

def day_trippers():
    """
    Look at day trips by origin and dest - focus on odd combos. Then look at
    sample. Qual.
    inputs: commuters, grid12_seen_in_day and grid12_home_location
    Count up the grid12_seen_in_day which are not commuters.
    lbs_s2_etl.grid12_daytrips_detail
    """
    
    
    
    

def main():
    """
    Check SANITY of:

    * international local SIM
      - proportion of internationals
      - sample check examples (inc a couple of animations)
    * number of international tourists
    * home origin
      - IMSIs per region cf regional populations
      - how many change home origin a lot?
      - how much tourism FROM a region is actually just people moving around?
      - how much itinerant living is there in NZ of people with domestic homes?
        (and double check whether we strip out all internationals properly)
    * commuting (possibly mis-located rtis - bouncing back and forth)
      - look at any popular commute zones e.g. Pokeno to Auckland? Too hard for
        benefit. Looking at sample in detail first i.e. going qual.
    * day tripping to obscure places from distant places
      - look at sample - how many the result of crazy leaps and roaming?
    * relationship between daily and monthly counts (rollup or uniques?)
    * extrapolations
      - domestic - cf with Camila's more detailed numbers
      - international

    * Al's number checks e.g. expecting same number of IMSIs across a transformation
    * Jing checking aggregation and both sides of API
    """
    extremes = [
        {'dest': 'Auckland RTO', 'home': 'Dunedin RTO'},
    ]
    check_alleged_commuter(imsi=8027953686920946790, date_str_tues='2017-10-10',
        extremes=extremes)

#     frequent_region_back_n_forth_sanity(date_str_tues='2017-10-10',
#         extremes_only=True)
    
#     utils.report_on_imsi(imsi=8027953686920946790,
#          min_date_str='2017-10-01', max_date_str='2017-10-02'); return
#     flow.FlowMap.make_imsi_travel_video(imsi=8027953686920946790,
#         min_date_str='2017-10-02', max_date_str='2017-10-15', fps=4,
#         display=False)
    
    
#     utils.report_on_imsi(imsi=4383746858415238637,
#         min_date_str='2017-03-23', max_date_str='2017-03-25'); return  ## 02-26
    #utils.report_on_imsi(imsi=633810881558904185,
    #    min_date_str='2017-09-26', max_date_str='2017-10-02')
    
    
    #international_local_sim_explore(use_stale=False)
    #product_int_year = international_total_check(year=2016, use_stale=True)
    #international_local_sim_tot_check(year=2016,
    #    product_int_year=product_int_year, use_stale=True)
    #home_location_popn_check(use_stale=True)
    #international_local_sim_flow_videos()
    #itinerants_identified()
    #indiv_itinerant_report(imsi=-1852062520891644606)
    
#     utils.report_on_imsi(-1852062520891644606, '', ''); return
#     
#     imsi = 2153395755593234888
#     indiv_itinerant_report(imsi); return
#     utils.report_on_imsi(imsi, '', ''); return
#     
#     imsi = 2407937436528533967
#     indiv_itinerant_report(imsi); return
#     utils.report_on_imsi(imsi, '', ''); return

if __name__ == '__main__':
    main()
