#! /usr/bin/env python3
from datetime import datetime

import sys
sys.path.insert(0, '../voyager_shared')
import conf #@UnresolvedImport
import utils #@UnresolvedImport

def waitaki_visitor_spike_relocation_detector(con_local, cur_local, cur_rem):
    from_rto = conf.WEST_COAST_RTO
    to_rto = conf.WAITAKI_RTO
    from_YYYYmmdd = '20150306'
    to_YYYYmmdd = '20150310'
    from_date_str = (datetime.strptime(from_YYYYmmdd, '%Y%m%d')
        .strftime('%Y-%m-%d'))
    dodgy_rtis = utils.FindRelocations.possible_relocations_from_fake_visits(
        con_local, cur_local, cur_rem, from_rto, from_YYYYmmdd, to_rto,
        to_YYYYmmdd)
    print(len(dodgy_rtis))
    (start_datetime_str,
     end_datetime_str) = utils.Dates.date_str_to_datetime_str_range(
         from_date_str)
    for n, rti in enumerate(dodgy_rtis, 1):
        utils.FindRelocations.get_likely_location_dts(cur_local,
            start_datetime_str, end_datetime_str, rti, rtis_to_exclude=None)
        if n % 20 == 0:
            input("Press any key to continue: ")

def _detect_swapped_locations(cur_local, rtis, possible_true_rto, date_str,
        lbl='from', skip_after_rti=None):
    n_rtis = len(rtis)
    print("suspect_{}_rtis: {:,}".format(lbl, n_rtis))
    skip_rti = bool(skip_after_rti)
    (start_datetime_str,
     end_datetime_str) = utils.Dates.date_str_to_datetime_str_range(date_str)
    for n, rti in enumerate(rtis, 1):
        """
        Are their locations (from or to) actually in the other location (to or
        from respectively) which would explain the fake tourists?
        """
        if skip_rti:
            if rti == skip_after_rti:
                skip_rti = False
            continue  ## until we get there
        print("Processing {}_rti {} ({:,} out of {:,})".format(lbl, rti, n,
            n_rtis))
        utils.FindRelocations.get_likely_location_dts(cur_local,
            start_datetime_str, end_datetime_str, rti,
            restrict_to_rto=possible_true_rto, smallfry_limit=100)  ## so we only have to look at cases where the 'from' rti is in the 'to' rto instead of where it should be

def day_trip_relocation_detector(con_local, cur_local, cur_rem,
        skip_after_from_rti=None, skip_after_to_rti=None):
    """
    skip_after_from_rti -- start processing rti's after this one (probably the
    last one completed before stopping/crashing etc.
    """

    '''
    from_rto = conf.WEST_COAST_RTO
    to_rto = conf.WAITAKI_RTO
    from_YYYYmmdd = '20150306'
    to_date_str = '2015-03-10'

    from_rto = conf.WAIKATO_RTO
    to_rto = conf.LAKE_TAUPO_RTO
    from_YYYYmmdd = '20150506'
    to_date_str = '2015-05-14'

    from_rto = conf.CANT_RTO
    to_rto = conf.NORTHLAND_RTO
    from_YYYYmmdd = '20151110'
    to_date_str = '2015-11-28'
    
    from_rto = conf.CANT_RTO
    to_rto = conf.NORTHLAND_RTO
    from_YYYYmmdd = '20151120'
    to_date_str = '2015-11-28'
    '''

    from_rto = conf.HAWKES_BAY_RTO
    to_rto = conf.TARANAKI_RTO
    from_YYYYmmdd = '20170317'
    to_date_str = '2017-03-27'

    from_date_str = (datetime.strptime(from_YYYYmmdd, '%Y%m%d')
        .strftime('%Y-%m-%d'))
    suspect_from_rtis, suspect_to_rtis = utils.FindRelocations.get_tourist_rtis(
        con_local, cur_local, cur_rem, from_rto, from_YYYYmmdd, to_rto,
        to_date_str)
    _detect_swapped_locations(cur_local, rtis=suspect_from_rtis,
        possible_true_rto=to_rto, date_str=from_date_str, lbl='from',
        skip_after_rti=skip_after_from_rti)  ## looking for 'froms' which are actually in 'to' location
    _detect_swapped_locations(cur_local, rtis=suspect_to_rtis,
        possible_true_rto=from_rto, date_str=to_date_str, lbl='to',
        skip_after_rti=skip_after_to_rti)  ## looking for 'tos' which are actually in 'from' location

def investigate_rti_locations(cur_local, rti, date_strs):
    for date_str in date_strs:
        (start_datetime_str,
        end_datetime_str) = utils.Dates.date_str_to_datetime_str_range(date_str)
        utils.FindRelocations.get_likely_location_dts(cur_local,
                start_datetime_str, end_datetime_str, rti)

def main():
    unused_com_rem, cur_rem = utils.Pg.get_rem()
    con_local, cur_local, unused_cur_local = utils.Pg.get_local()
    '''
    rti = 37823
    date_strs = ['2015-10-01', '2015-11-20', '2015-11-28', '2016-01-20', ]
    investigate_rti_locations(cur_local, rti, date_strs); return
    '''
    skip_after_from_rti = -1  ## -1 will never be matched so can enter if wanting to skip all. None means do all.
    skip_after_to_rti = 372615
    day_trip_relocation_detector(con_local, cur_local, cur_rem,
        skip_after_from_rti, skip_after_to_rti)


if __name__ == '__main__':
    main()
