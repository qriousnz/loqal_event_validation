#! /usr/bin/env python3

"""
What's the face validity like for our arrival, visit, and visitors numbers.

Focus on days. Annotate with events e.g. concerts, cruise ships, weekends,
holidays etc. Broad correlation?
"""
from datetime import datetime, timedelta
import sqlite3 as sqlite
from sqlite3 import Row #@UnresolvedImport
from urllib.parse import quote as urlquote
from webbrowser import open_new_tab

import matplotlib.pyplot as plt

import sys
sys.path.insert(0, '../voyager_shared')
import conf #@UnresolvedImport
from mpl import Humanise, Mpl #@UnresolvedImport
from utils import str2dt #@UnresolvedImport

MAIN_IMG_HEIGHT = 25

X_OFFSET = timedelta(hours=36)
YEARS = [2016, 2017, ]  ## TODO -- softwire years from db call

(nat_hol_dts, reg_hol_dts, hol_dt2dets,
 nat_event_dt2dets,
 reg_event_mappings) = Humanise.prep_date_event_mappings()

def make_domestic_origin_region_images(title, region_name,
        daily_data_dom_origin_regs, total_height):
    """
    Make the smaller charts - one per domestic region of visitor origin.
    """
    img_dets = []
    for (dom_origin_reg, unused,
             daily_data_dom_origin_reg) in daily_data_dom_origin_regs:
        ## General
        x = [data_dic['dt'] for data_dic in daily_data_dom_origin_reg]
        y = [data_dic['freq'] for data_dic in daily_data_dom_origin_reg]
        max_y = max(y)
        shrink_by = total_height/max_y
        if shrink_by > 25:  ## only interested if some data to look at
            continue
        fig = plt.figure(figsize=(180, MAIN_IMG_HEIGHT/shrink_by))
        ax = fig.add_subplot(111)
        Mpl.set_ticks(ax)
        ax.plot(x, y, linewidth=4, color=conf.DUSK_GREEN)
        ax.set_yticks([max_y, ])
        reg_title = dom_origin_reg
        for loc in ['left', 'center', 'right']:
            ax.set_title(reg_title, fontsize=42, loc=loc, fontweight='bold',
                verticalalignment='bottom')
        ax.set_ylim([0, total_height/shrink_by])
        Mpl.set_axis_labels(ax)
        Mpl.add_year_vlines(ax, X_OFFSET, x, rotate=True, lower_propn_line=0,
            lower_propn_text=0.9, fontsize=24)
        ## Create
        img_name = "{} from {} - {}.png".format(title, dom_origin_reg,
            region_name)
        img_fpath = "reports/images/charts/{}".format(img_name)
        img_rel_path = "images/charts/{}".format(img_name)
        fig.savefig(img_fpath, bbox_inches='tight')
        plt.close(fig)
        img_dets.append({'rel_path': img_rel_path})  ## don't want title taking up space
    return img_dets

def make_images(title, region_name, daily_data_total, daily_data_intl,
        daily_data_dom_origin_regs):
    """
    Make image including total for region (coloured bars) and internationals
    (a line overlayed) with domestic origin regions as individual charts with
    own heights.
    """
    img_dets = []
    ## 1) Regional total chart
    fig = plt.figure(figsize=(180, MAIN_IMG_HEIGHT))
    ax = fig.add_subplot(111)
    Mpl.set_ticks(ax)
    x_total = [data_dic['dt'] for data_dic in daily_data_total]
    min_x = daily_data_total[0]['dt']
    data_start_dt = daily_data_total[0]['dt']
    data_end_dt = daily_data_total[-1]['dt']
    y_total = [data_dic['freq'] for data_dic in daily_data_total]
    max_y_total = max(y_total)
    (std_offset, small_std_offset,
     tiny_std_offset) = Mpl.get_y_offsets(max_y_total)
    gap_as_fraction = 0.1
    height = max_y_total*(1 + gap_as_fraction)
    humanised_total = Humanise.annotate_and_colour(daily_data_total,
        region_name, nat_event_dt2dets, reg_event_mappings, hol_dt2dets,
        max_y_total)
    ## Bars
    ax.bar(x_total, y_total, 0.9, color=humanised_total['colours4dts'],
        edgecolor="none", align='center')  ## align on centre so aligns with intl line
    ## Daylight savings
    ## only add if data for that period displayed otherwise lines on a chart but no bar for that part i.e. doh!
    for start, end in conf.NON_DAYLIGHT_SAVINGS:
        start_dt = str2dt(start)
        end_dt = str2dt(end)
        if start_dt >= data_start_dt and end_dt <= data_end_dt:
            ax.axvspan(start_dt, end_dt, facecolor='grey', alpha=0.15)
            ax.text(start_dt, height, 'DST ends ', ha='left', va='top',
                fontsize=24, rotation=90)
            ax.text(end_dt, height, 'DST starts ', ha='left', va='top',
                fontsize=24, rotation=90)
    ## Annotations and annotation legend
    legend_x_offset = 2*X_OFFSET
    col_x_offset = legend_x_offset + (2*X_OFFSET)
    Mpl.add_annotation_legend(ax, legend_x_offset, col_x_offset, min_x,
        small_std_offset, tiny_std_offset)
    Mpl.add_mpl_annotations(ax, humanised_total['dts_with_annotations'],
        X_OFFSET, std_offset, small_std_offset, tiny_std_offset)
    ## International line and legend
    x_intl = [data_dic['dt'] for data_dic in daily_data_intl]
    y_intl = [data_dic['freq'] for data_dic in daily_data_intl]
    ax.plot(x_intl, y_intl, linewidth=4, color='black')
    first_intls = [x['freq'] for x in daily_data_intl[:50]]
    min_first_intls = min(first_intls)
    x_intl_leg = daily_data_intl[0]['dt'] + timedelta(days=2)
    y_intl_leg = min_first_intls - (std_offset/10)
    ax.text(x_intl_leg, y_intl_leg, 'International', ha='left', va='top',
        fontsize=18, fontweight='bold')
    ## General
    total_title = "{} to {} by date".format(title, region_name)
    Mpl.set_titles(ax, total_title)
    ax.set_xlim(x_total[0], x_total[-1])
    ax.set_ylim([0, height])  ## year vlines relies on ylim being set
    Mpl.add_year_vlines(ax, X_OFFSET, x_total, rotate=True,
        lower_propn_line=0.935, lower_propn_text=0.99, fontsize=28)
    Mpl.set_axis_labels(ax)
    img_name = "{} - {}.png".format(title, region_name)
    img_fpath = "reports/images/charts/{}".format(img_name)
    img_rel_path = "images/charts/{}".format(img_name)
    fig.savefig(img_fpath, bbox_inches='tight')
    plt.close(fig)
    img_dets.append({'rel_path': img_rel_path})
    ## 2) Domestic origin regions
    reg_img_dets = make_domestic_origin_region_images(title, region_name,
        daily_data_dom_origin_regs, height)
    img_dets.extend(reg_img_dets)
    return img_dets

def map_data(filtered_regions, db, tbl, label, scaled=False,
        AND_date_filter=''):
    """
    Add weekend, holiday, and location label data to charts where possible
    to make it easier to check face validity.
    """
    if scaled:
        suffix = " (scaled)"
        fldname = "arrivals"
    else:
        suffix = " (unscaled)"
        fldname = "arrivals_raw"
    title = label + suffix
    html = [conf.HTML_START_TPL % {'title': title, 'more_styles': ''}, ]
    con = sqlite.connect(db) #@UndefinedVariable
    con.row_factory = Row
    tpl_unfiltered = """\
    SELECT
      date,
        SUM({fldname})
      AS freq
    FROM {tbl}
    {{}}
    GROUP BY date
    ORDER BY date
    """.format(fldname=fldname, tbl=tbl)
    tpl_region_daily_data = tpl_unfiltered.format("WHERE region_id = ? {}"
        .format(AND_date_filter))
    tpl_intl_daily_data = tpl_unfiltered.format("""\
    WHERE origin_type = 'international'
    AND region_id = ?
    {}""".format(AND_date_filter))
    ## getting short list of home provinces relevant to specific region
    tpl_dom_origin_regs = """\
    SELECT home_province, SUM(arrivals_raw) AS tot_arrivals
    FROM {tbl}
    WHERE region_id = ?
    AND home_province IS NOT NULL
    GROUP BY home_province
    ORDER BY tot_arrivals DESC
    """.format(tbl=tbl)
    tpl_dom_origin_reg_daily_data = tpl_unfiltered.format("""\
    WHERE home_province = ?
    AND region_id = ?
    {}""".format(AND_date_filter))
    html.append("<h2>CONTENTS</h2>")
    html.append("<br>\n".join("<a href='#{}'>{}</a>"
        .format(urlquote(region_name), region_name)
        for region_name in conf.RTO_MAP.values()
        if region_name in filtered_regions))
    for region_id, region_name in conf.RTO_MAP.items():
        if region_name not in filtered_regions: continue
        html.append("<a name='{}'/><h2>{}</h2>".format(
            urlquote(region_name), region_name))
        ## REGION
        cur_region = con.execute(tpl_region_daily_data, (region_id, ))
        raw_daily_data_region = cur_region.fetchall()
        has_data = bool(raw_daily_data_region)
        if not has_data:
            html.append("No daily data for {}\n\n{}".format(region_name,
                tpl_region_daily_data))
            continue
        if has_data:
            daily_data_region = Humanise.enrich_dates(raw_daily_data_region,
                nat_event_dt2dets, nat_hol_dts, reg_hol_dts)
            ## INTERNATIONAL ORIGINS
            cur_intl = con.execute(tpl_intl_daily_data, (region_id, ))
            raw_daily_data_intl = cur_intl.fetchall()
            daily_data_intl = Humanise.enrich_dates(raw_daily_data_intl,
                nat_event_dt2dets, nat_hol_dts, reg_hol_dts)
            ## DOMESTIC ORIGINS (REGIONS)
            ## Which domestic origin regions?
            cur_dom_origin_regs = con.execute(tpl_dom_origin_regs,
                (region_id, ))
            dom_origin_regs = cur_dom_origin_regs.fetchall()
            ## Data for each home province
            daily_data_dom_origin_regs = []
            for dom_origin_reg, total_arrivals in dom_origin_regs:
                if total_arrivals < 500*100:  ## avoid processing datasets we're almost certainly going to not display (because scaled too short compared with that of the total chart)
                    continue
                cur_dom_origin_reg = con.execute(tpl_dom_origin_reg_daily_data,
                    (dom_origin_reg, region_id))
                raw_daily_data_dom_origin_reg = cur_dom_origin_reg.fetchall()
                daily_data_dom_origin_reg = Humanise.enrich_dates(
                    raw_daily_data_dom_origin_reg, nat_event_dt2dets,
                    nat_hol_dts, reg_hol_dts)
                daily_data_dom_origin_regs.append((dom_origin_reg,
                    total_arrivals, daily_data_dom_origin_reg))
            ## CHARTING
            imgs_dets = make_images(title, region_name, daily_data_region,
                daily_data_intl, daily_data_dom_origin_regs)
            for img_dets in imgs_dets:
                html.append("<img width='3200px' padding=0 margin=0 src='{}'>"
                    .format(img_dets['rel_path']))
            print('Made {} charts for "{}"'.format(title, region_name))
    content = "\n".join(html)
    fpath = ("/home/gps/Documents/tourism/validation/reports/{}_validation.html"
        .format(title.replace(" ", "_")))
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))

if __name__ == '__main__':

    date_filt = ''
    
    #date_filt = "AND date BETWEEN '20150101' AND '20151231'"

    #filtered_regions = [conf.WAITAKI_RTO, ] #, conf.HAWKES_BAY_RTO, conf.WANAKA_RTO, conf.MILFORD_SOUND_CUSTOM]
    filtered_regions = conf.RTO_MAP.values()

    day_trips = 'day_trips_view_daily'
    visitors = 'visitor_view_daily'
    visits = 'visits_view_daily'

    as_at_date = datetime.today().strftime('%Y-%m-%d')
    arrivals_and_visits = 'arrivals_and_visits_20.db'
    reports_dets = [
        {'db': arrivals_and_visits, 'tbl': day_trips, 'label': 'Day Trips {}'.format(as_at_date), 'scaled': True, 'AND_date_filter': date_filt},
        {'db': arrivals_and_visits, 'tbl': visitors, 'label': 'Visitors {}'.format(as_at_date), 'scaled': True, 'AND_date_filter': date_filt},
        {'db': arrivals_and_visits, 'tbl': visits, 'label': 'Visits {}'.format(as_at_date), 'scaled': True, 'AND_date_filter': date_filt},
    ]
    #filtered_reports_dets = reports_dets[:1]
    #reports_dets = [
    #    {'source': 'visits_view_daily', 'label': 'Visits test', 'scaled': True},
    #]
    filtered_reports_dets = reports_dets
    for filtered_report_dets in filtered_reports_dets:
        map_data(filtered_regions, **filtered_report_dets)
