#! /usr/bin/env python3

"""
The data pipeline ends up with an arrivals value of 10,409 on 20150309 (and an
arrivals_raw of 4195). Normally it is about 350 arrivals_raw per day at that
time. I.e. 12x normal quantity.

SELECT * 
FROM visits_view_daily
WHERE reporting_rto = 'Waitaki RTO'
AND home_province = 'West Coast RTO'
AND date = 20150309;

Can we find a large increase (e.g. 12x) on that date for West Coast

For each IMSI (SIM / person) we have s2_12_index and s2_12_home
e.g. 213 and 213 = at_home True (and category 12213.0213
1 = domestic, 2 = at home, 213 = home 213 = dest)

So .....

we are interested in all records where a home s2_12 in West Coast RTO and a dest
s2_12 in Waitaki RTO. Compare 8th of March 2015 with some 'normal' days
surrounding it.
"""

from collections import defaultdict, OrderedDict
import os
from pprint import pformat as pf, pprint as pp

import pandas as pd

import sys
sys.path.insert(0, '../voyager_shared')
import conf #@UnresolvedImport
import utils #@UnresolvedImport
from db import Pg #@UnresolvedImport
from area import Area, Deprecated as DepArea #@UnresolvedImport
from dates import Dates #@UnresolvedImport
import location_sources as locsrcs #@UnresolvedImport
import report_relocations as reprels #@UnresolvedImport
from folium_map import FoliumMap #@UnresolvedImport

GET_FRESH_SRC = False
IMPORT_CSVS = False
SRC_DEST_CONNECTIONS = 'src_dest_connections'

ROOT = '/home/gps/Documents/tourism/validation/'
hdf5_first_in_day_categorized_fnames = [
    'first_in_day_categorized_20150305',
    'first_in_day_categorized_20150306',
    'first_in_day_categorized_20150307',
    'first_in_day_categorized_20150308',
    'first_in_day_categorized_20150309',
]
hdf5_max_by_imsi_35_days_fnames = [
    'max_by_imsi_35_days_ending_20150302',
    'max_by_imsi_35_days_ending_20150309',
]
hdf5_first_in_day_fnames = [
    'first_in_day_20150305',
    'first_in_day_20150306',
    'first_in_day_20150307',
    'first_in_day_20150308',
    'first_in_day_20150309',
]

def hdf5_to_csv(hdf5_fnames, root_key):
    csvs_dets = []
    hdf5_fpaths = [os.path.join(ROOT, fname) for fname in hdf5_fnames]
    for hdf5_fpath in hdf5_fpaths:
        store = pd.HDFStore(hdf5_fpath)
        data = store[root_key]
        csv_fpath = "{}.csv".format(hdf5_fpath)
        datestr = hdf5_fpath[-8:]
        csvs_dets.append({
            'csv_fpath': csv_fpath,
            'datestr': datestr})
        data.to_csv(csv_fpath, header=False)
        store.close()
    return csvs_dets

def first_in_day_categorized_csv2pg(con, cur, csv_fpath, tblname):
    Pg.drop_tbl(con, cur, tblname)
    sql_make_tbl = """\
    CREATE TABLE {tblname} (
      id integer,
      imsi_rti double precision,
      s2_12_index integer,
      prior_s2_12_index double precision,
      staying_from_prior boolean,
      s2_12_home integer,
      mcc integer,
      week_first_seen date,
      local_sim_flag boolean,
      at_home boolean,
      is_domestic boolean,
      category double precision,
      PRIMARY KEY(id)
    )
    """.format(tblname=tblname)
    cur.execute(sql_make_tbl)
    con.commit()
    with open(csv_fpath) as f:
        cur.copy_from(f, tblname, sep=',')
    con.commit()

def max_in_imsi_35_days_csv2pg(con, cur, csv_fpath, tblname):
    Pg.drop_tbl(con, cur, tblname)
    sql_make_tbl = """\
    CREATE TABLE {tblname} (
      id integer,
      imsi_rti double precision,
      s2_12_index integer,
      count integer,
      PRIMARY KEY(id)
    )
    """.format(tblname=tblname)
    cur.execute(sql_make_tbl)
    con.commit()
    with open(csv_fpath) as f:
        cur.copy_from(f, tblname, sep=',')
    con.commit()

def first_in_day_csv2pg(con, cur, csv_fpath, tblname):
    Pg.drop_tbl(con, cur, tblname)
    sql_make_tbl = """\
    CREATE TABLE {tblname} (
      id integer,
      imsi_rti double precision,
      s2_12_index integer,
      PRIMARY KEY(id)
    )
    """.format(tblname=tblname)
    cur.execute(sql_make_tbl)
    con.commit()
    with open(csv_fpath) as f:
        cur.copy_from(f, tblname, sep=',')
    con.commit()

def csvs2pg(con, cur, csvs_dets, root_name, tblmaker_fn):
    tblnames = []
    for csv_dets in csvs_dets:
        tblname = "{}_{}".format(root_name, csv_dets['datestr'])
        tblmaker_fn(con, cur, csv_dets['csv_fpath'], tblname)
        tblnames.append(tblname)
    return tblnames

def first_in_day_categorized_analysis(cur, src_region, dest_region, tblnames,
        s2_12_to_region_name):
    """
    Source for arrivals_and_visits.db

    Basically identifies home and dest per person which we then use to work out
    where someone was a tourist (the dest e.g. Waitaki RTO) and the region
    they're from (the src e.g. West Coast RTO). The spike will manifest itself
    as large numbers on date where src = 'West Coast RTO' and dest =
    'Waitaki RTO'.

    For each IMSI (SIM / person) we have s2_12_index and s2_12_home
    e.g. 213 and 213 = at_home True (and category 12213.0213
    1 = domestic, 2 = at home, 213 = home 213 = dest)

    So .....

    we are interested in all records where a home s2_12 in West Coast RTO and a
    dest s2_12 in Waitaki RTO. Compare 8th and 9th of March 2015.

    SELECT * 
    FROM visitor_view_daily
    WHERE reporting_rto = 'Waitaki RTO'
    AND home_province = 'West Coast RTO'
    AND date BETWEEN 20150301 AND 20150314;

    date      arrivals  reporting_rto  home_province       extrap             arrivals_raw
    --------------------------------------------------------------------------------------
    20150308  9262     "Waitaki RTO"   "West Coast RTO"    2.46778435239974   3753
    20150309  1047     "Waitaki RTO"   "West Coast RTO"    2.43220338983051    430
    """
    src_s2_12s_clause = DepArea.get_s2_12s_clause(s2_12_to_region_name,
        region_name=src_region)
    dest_s2_12s_clause = DepArea.get_s2_12s_clause(s2_12_to_region_name,
        region_name=dest_region)
    sql_imsi_rtis_tpl = """\
    SELECT COUNT(*) AS n_imsi_rtis FROM (
      SELECT DISTINCT IMSI_rti
      FROM {{}}
      WHERE s2_12_home IN {src_s2_12s_clause}
      AND s2_12_index IN {dest_s2_12s_clause}
    ) AS imsis
    """.format(src_s2_12s_clause=src_s2_12s_clause,
        dest_s2_12s_clause=dest_s2_12s_clause)
    for tblname in tblnames:
        cur.execute(sql_imsi_rtis_tpl.format(tblname))
        n_imsi_rtis = cur.fetchone()['n_imsi_rtis']
        print(tblname, n_imsi_rtis)

def first_in_day_categorized_analyses(con_local, cur_local, src_region,
        dest_region, s2_12_to_region_name):
    if GET_FRESH_SRC:
        csvs_dets = hdf5_to_csv(hdf5_first_in_day_categorized_fnames,
            root_key='matched')
    else:
        utils.warn("Not extracting fresh data from HDF5 "
            "files")
        csvs_dets = [
            {'csv_fpath': '{}first_in_day_categorized_20150305.csv'.format(ROOT),
             'datestr': '20150305'},
            {'csv_fpath': '{}first_in_day_categorized_20150306.csv'.format(ROOT),
             'datestr': '20150306'},
            {'csv_fpath': '{}first_in_day_categorized_20150307.csv'.format(ROOT),
             'datestr': '20150307'},
            {'csv_fpath': '{}first_in_day_categorized_20150308.csv'.format(ROOT),
             'datestr': '20150308'},
            {'csv_fpath': '{}first_in_day_categorized_20150309.csv'.format(ROOT),
             'datestr': '20150309'},
        ]
    root_name = 'matched'
    if IMPORT_CSVS:
        tblnames = csvs2pg(con_local, cur_local, csvs_dets, root_name,
            tblmaker_fn=first_in_day_categorized_csv2pg)
    else:
        tblnames = [
            '{}_20150305'.format(root_name),
            '{}_20150306'.format(root_name),
            '{}_20150307'.format(root_name),
            '{}_20150308'.format(root_name),
            '{}_20150309'.format(root_name),
        ]
    first_in_day_categorized_analysis(cur_local, src_region, dest_region,
        tblnames, s2_12_to_region_name)

def max_by_imsi_35_days_analysis(cur, src_region, dest_region, tblnames,
        s2_12_to_region_name):
    src_s2_12s_clause = DepArea.get_s2_12s_clause(s2_12_to_region_name,
        region_name=src_region)
    sql_src_imsis_tpl = """\
    SELECT COUNT(*) AS
    freq
    FROM {{}}
    WHERE s2_12_index IN {src_s2_12s_clause}
    """.format(src_s2_12s_clause=src_s2_12s_clause)
    for tblname in tblnames:
        cur.execute(sql_src_imsis_tpl.format(tblname))
        n_imsi_rtis = cur.fetchone()['freq']
        print(tblname, n_imsi_rtis)

def max_by_imsi_35_days_analyses(con_local, cur_local, src_region, dest_region,
        s2_12_to_region_name):
    """
    imsi_rti, s2_12_index, count
    """
    if GET_FRESH_SRC:
        csvs_dets = hdf5_to_csv(hdf5_max_by_imsi_35_days_fnames,
            root_key='max_by_imsi')
    else:
        utils.warn("Not extracting fresh data from HDF5 "
            "files")
        csvs_dets = [
            {'csv_fpath': '{}max_by_imsi_35_days_ending_20150302.csv'.format(ROOT),
             'datestr': '20150302'},
            {'csv_fpath': '{}max_by_imsi_35_days_ending_20150309.csv'.format(ROOT),
             'datestr': '20150309'},
        ]
    root_name = 'max_by_imsi'
    if IMPORT_CSVS:
        tblnames = csvs2pg(con_local, cur_local, csvs_dets, root_name,
            tblmaker_fn=max_in_imsi_35_days_csv2pg)
    else:
        tblnames = [
            '{}_20150302'.format(root_name),
            '{}_20150309'.format(root_name),
        ]
    max_by_imsi_35_days_analysis(cur_local, src_region, dest_region, tblnames,
        s2_12_to_region_name)

def first_in_day_analysis(cur, src_region, dest_region, tblnames,
        s2_12_to_region_name, audit=False):
    """
    Assumptions - first_in_day has one record per IMSI which indicates the s2_12
    that the IMSI started with.

    Note - date filtering happens by choice of tables used (one table per day)
    """
    src_s2_12s_clause = DepArea.get_s2_12s_clause(s2_12_to_region_name,
        region_name=src_region)
    dest_s2_12s_clause = DepArea.get_s2_12s_clause(s2_12_to_region_name,
        region_name=dest_region)
    ## Freq of IMSI's which started in src_region
    sql_src_imsis_tpl = """\
    SELECT COUNT(*) AS
    freq
    FROM {{}}
    WHERE s2_12_index IN {src_s2_12s_clause}
    """.format(src_s2_12s_clause=src_s2_12s_clause)
    ## Freq of IMSI's which started in dest region
    sql_dest_imsis_tpl = """\
    SELECT COUNT(*) AS
    freq
    FROM {{}}
    WHERE s2_12_index IN {dest_s2_12s_clause}
    """.format(dest_s2_12s_clause=dest_s2_12s_clause)
    ## Actual IMSI's which started in src_region
    sql_imsi_in_src_tpl = """\
    SELECT IMSI_rti
    FROM {{}}
    WHERE s2_12_index IN {src_s2_12s_clause}
    """.format(src_s2_12s_clause=src_s2_12s_clause)
    ## Actual IMSI's which started in dest_region
    sql_imsi_in_dest_tpl = """\
    SELECT IMSI_rti
    FROM {{}}
    WHERE s2_12_index IN {dest_s2_12s_clause}
    """.format(dest_s2_12s_clause=dest_s2_12s_clause)
    prev_tblname = None
    for tblname in tblnames:
        print("\n")
        ## some background description of numbers - not the actual thing we are interested in but might show something unusual
        for lbl, tpl in [
                (src_region, sql_src_imsis_tpl),
                (dest_region, sql_dest_imsis_tpl),
                ]:
            cur.execute(tpl.format(tblname))
            n_imsi_rtis = cur.fetchone()['freq']
            print(lbl, tblname, n_imsi_rtis)
        if prev_tblname:  ## i.e. we can do a comparison between two dates (dated tables)
            ## Look at data for src in first date and dest in the following date
            ## WHERE same IMSI i.e. IMSI in src region on day 1 and dest region
            ## on day 2 (note - may be in both regions on first day etc as long
            ## as pattern includes src on 1 and dest on 2)
            sql_imsi_was_in_src = sql_imsi_in_src_tpl.format(prev_tblname)
            sql_imsi_is_in_dest = sql_imsi_in_dest_tpl.format(tblname)
            sql_from_src_to_dest = """\
            SELECT
              COUNT(*) AS
            freq
            FROM
            ({sql_imsi_was_in_src}) AS src
            INNER JOIN
            ({sql_imsi_is_in_dest}) AS dest
            USING(IMSI_rti)
            """.format(sql_imsi_was_in_src=sql_imsi_was_in_src,
                sql_imsi_is_in_dest=sql_imsi_is_in_dest)
            if audit: print(sql_from_src_to_dest)
            cur.execute(sql_from_src_to_dest)
            n_imsi_rtis = cur.fetchone()['freq']
            print('From {} to {}'.format(src_region, dest_region),
                prev_tblname, tblname, n_imsi_rtis)
        prev_tblname = tblname

def first_in_day_analyses(con_local, cur_local, src_region, dest_region,
        s2_12_to_region_name, audit=False):
    if GET_FRESH_SRC:
        csvs_dets = hdf5_to_csv(hdf5_first_in_day_fnames, root_key='data')
    else:
        utils.warn("Not extracting fresh data from HDF5 "
            "files")
        csvs_dets = [
            {'csv_fpath': '{}first_in_day_20150305.csv'.format(ROOT),
             'datestr': '20150305'},
            {'csv_fpath': '{}first_in_day_20150306.csv'.format(ROOT),
             'datestr': '20150306'},
            {'csv_fpath': '{}first_in_day_20150307.csv'.format(ROOT),
             'datestr': '20150307'},
            {'csv_fpath': '{}first_in_day_20150308.csv'.format(ROOT),
             'datestr': '20150308'},
            {'csv_fpath': '{}first_in_day_20150309.csv'.format(ROOT),
             'datestr': '20150309'},
        ]
    root_name = 'data'
    if IMPORT_CSVS:
        tblnames = csvs2pg(con_local, cur_local, csvs_dets, root_name,
            tblmaker_fn=first_in_day_csv2pg)
    else:
        tblnames = [
            '{}_20150305'.format(root_name),
            '{}_20150306'.format(root_name),
            '{}_20150307'.format(root_name),
            '{}_20150308'.format(root_name),
            '{}_20150309'.format(root_name),
        ]
    first_in_day_analysis(cur_local, src_region, dest_region, tblnames,
        s2_12_to_region_name, audit)

def make_src_to_dest_region_connection_tbl(con_local, cur_local, dates,
        src_region, dest_region, s2_12_to_region_name):
    """
    Filter to only include everything for relevant dates and cell_id_rti's. Will
    include more than we use but will enable us to check our results make sense.

    Need to include multiple dates so can do multiple comparisons - at least one
    additional to provide context for the 'odd' one.
    """
    cell_id_rtis_in_src_region = DepArea.get_cell_id_rtis_in_region(
        src_region, s2_12_to_region_name)
    cell_id_rtis_in_dest_region = DepArea.get_cell_id_rtis_in_region(
        dest_region, s2_12_to_region_name)
    all_cell_id_rtis = sorted(list(set(cell_id_rtis_in_src_region
        + cell_id_rtis_in_dest_region)))
    all_cell_id_rtis_clause = Pg.nums2clause(all_cell_id_rtis)
    Pg.drop_tbl(con_local, cur_local, SRC_DEST_CONNECTIONS)
    sql_make_tbl = """\
    CREATE TABLE {src_dest_connections} (
      id SERIAL,
      cell_id_rti integer,
      imsi_rti bigint,
      date text,
      PRIMARY KEY(id)
    )
    """.format(src_dest_connections=SRC_DEST_CONNECTIONS)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    unused_con_rem, cur_rem = Pg.get_rem()
    sql_src_and_dest = """\
    SELECT cell_id_rti, imsi_rti, date
    FROM lbs_agg.fact_rti_summary
    WHERE cell_id_rti IN {all_cell_id_rtis_clause}
    AND date BETWEEN %s AND %s 
    ORDER BY date, imsi_rti, cell_id_rti
    """.format(all_cell_id_rtis_clause=all_cell_id_rtis_clause)
    cur_rem.execute(sql_src_and_dest, (dates[0], dates[-1]))
    sql_insert_tpl = """\
    INSERT INTO {src_dest_connections}
    (cell_id_rti, imsi_rti, date)
    VALUES(%s, %s, %s)
    """.format(src_dest_connections=SRC_DEST_CONNECTIONS)
    n = 1
    while True:
        row = cur_rem.fetchone()
        if not row:
            break
        cur_local.execute(sql_insert_tpl, tuple(row))
        if n % 25000 == 0:
            print("Added {:,} records to '{}'".format(n,
                SRC_DEST_CONNECTIONS))
        n += 1
    print("Added {:,} records to '{}'".format(n - 1, SRC_DEST_CONNECTIONS))
    con_local.commit()

def src_to_dest_region_connection_data(con_local, cur_local, dates, src_region,
        dest_region, s2_12_to_region_name, make_src=True):
    """
    For each date pair we look at all IMSI's which connected to a cell_id_rti
    inside the source region on the first date and the dest region on the second
    date.
    """
    if not make_src:
        utils.warn("Not making fresh source data - which "
            "will be a problem if you've extended the date ranges you're "
            "interested in")
    else:
        make_src_to_dest_region_connection_tbl(con_local, cur_local, dates,
            src_region, dest_region, s2_12_to_region_name)
    cell_id_rtis_in_src_region = DepArea.get_cell_id_rtis_in_region(
        src_region, s2_12_to_region_name)
    cell_id_rtis_in_src_region_clause = Pg.nums2clause(
        cell_id_rtis_in_src_region)
    cell_id_rtis_in_dest_region = DepArea.get_cell_id_rtis_in_region(
        dest_region, s2_12_to_region_name)
    cell_id_rtis_in_dest_region_clause = Pg.nums2clause(
        cell_id_rtis_in_dest_region)
    date_pairs = utils.dates2pairs(dates)
    for start, end in date_pairs:
        sql_src_to_dest = """\
        SELECT
          COUNT(*) AS
        freq
        FROM
        (
          SELECT DISTINCT imsi_rti
          FROM {src_dest_connections}
          WHERE
          (cell_id_rti IN {cell_id_rtis_in_src_region_clause} AND date = %s)
        ) AS src
        INNER JOIN
        (
          SELECT DISTINCT imsi_rti
          FROM {src_dest_connections}
          WHERE
          (cell_id_rti IN {cell_id_rtis_in_dest_region_clause} AND date = %s)
        ) AS dest
        USING(imsi_rti)
        """.format(src_dest_connections=SRC_DEST_CONNECTIONS,
            cell_id_rtis_in_src_region_clause=cell_id_rtis_in_src_region_clause,
            cell_id_rtis_in_dest_region_clause=cell_id_rtis_in_dest_region_clause)
        cur_local.execute(sql_src_to_dest, (start, end))
        n_src_then_dest = cur_local.fetchone()[0]  ## note - might be in both at both times, or src and dest then dest only etc. As long as in src and then in dest.
        print("{:,} IMSI_rti's in {} on {} then in {} on {}".format(
            n_src_then_dest, src_region, start, dest_region, end))

def src_to_dest_cell_id_rti_connection_data(con_local, cur_local, dates,
        src_region, dest_region, s2_12_to_region_name, make_src=True):
    """
    Anything odd in terms of a cell_id_rti becoming a much more important source
    or destination e.g. 300 in one date comparison and 1500 in the next?

    Note - each IMSI only has one 
    """
    analyse_src = True
    analyse_dest = True
    analyse_src_dest_pairs = True
    if not make_src:
        utils.warn("Not making fresh source data - which will be a problem if "
            "you've extended the date ranges you're interested in")
    else:
        make_src_to_dest_region_connection_tbl(con_local, cur_local, dates,
            src_region, dest_region, s2_12_to_region_name)
    cell_id_rtis_in_src_region = DepArea.get_cell_id_rtis_in_region(
        src_region, s2_12_to_region_name)
    cell_id_rtis_in_src_region_clause = Pg.nums2clause(
        cell_id_rtis_in_src_region)
    cell_id_rtis_in_dest_region = DepArea.get_cell_id_rtis_in_region(
        dest_region, s2_12_to_region_name)
    cell_id_rtis_in_dest_region_clause = Pg.nums2clause(
        cell_id_rtis_in_dest_region)
    date_pairs = utils.dates2pairs(dates)
    src_cell_id_rti_imsis = defaultdict(OrderedDict)
    dest_cell_id_rti_imsis = defaultdict(OrderedDict)
    src_dest_cell_id_rti_pair_imsis = defaultdict(OrderedDict)
    for start, end in date_pairs:
        if analyse_src:
            ## src
            sql_src_cell_id_rtis = """\
            SELECT
            cell_id_rti,
              COUNT(*) AS
            freq
            FROM (
              SELECT DISTINCT cell_id_rti, imsi_rti
              FROM {src_dest_connections}
              WHERE cell_id_rti IN {cell_id_rtis_in_src_region_clause}
              AND date = %s
            ) AS src
            GROUP BY cell_id_rti
            """.format(src_dest_connections=SRC_DEST_CONNECTIONS,
                cell_id_rtis_in_src_region_clause=\
                cell_id_rtis_in_src_region_clause)
            cur_local.execute(sql_src_cell_id_rtis, (start, ))
            for cell_id_rti, freq in cur_local.fetchall():
                src_cell_id_rti_imsis[cell_id_rti][start] = freq
        if analyse_dest:
            ## dest
            sql_dest_cell_id_rtis = """\
            SELECT
            cell_id_rti,
              COUNT(*) AS
            freq
            FROM (
              SELECT DISTINCT cell_id_rti, imsi_rti
              FROM {src_dest_connections}
              WHERE cell_id_rti IN {cell_id_rtis_in_dest_region_clause}
              AND date = %s
            ) AS dest
            GROUP BY cell_id_rti
            """.format(src_dest_connections=SRC_DEST_CONNECTIONS,
                cell_id_rtis_in_dest_region_clause=\
                cell_id_rtis_in_dest_region_clause)
            cur_local.execute(sql_dest_cell_id_rtis, (end, ))
            for cell_id_rti, freq in cur_local.fetchall():
                dest_cell_id_rti_imsis[cell_id_rti][end] = freq
        if analyse_src_dest_pairs:
            ## src to dest
            sql_src_to_dest_cell_id_rtis = """\
            SELECT
            src.cell_id_rti,
            dest.cell_id_rti,
              COUNT(*) AS
            freq
            FROM (
              SELECT DISTINCT cell_id_rti, imsi_rti
              FROM {src_dest_connections}
              WHERE cell_id_rti IN {cell_id_rtis_in_src_region_clause}
              AND date = %s
            ) AS src
            INNER JOIN
            (
              SELECT DISTINCT cell_id_rti, imsi_rti
              FROM {src_dest_connections}
              WHERE cell_id_rti IN {cell_id_rtis_in_dest_region_clause}
              AND date = %s
            ) AS dest
            USING(imsi_rti)
            GROUP BY src.cell_id_rti, dest.cell_id_rti
            """.format(src_dest_connections=SRC_DEST_CONNECTIONS,
                cell_id_rtis_in_src_region_clause=\
                cell_id_rtis_in_src_region_clause,
                cell_id_rtis_in_dest_region_clause=\
                cell_id_rtis_in_dest_region_clause)
            cur_local.execute(sql_src_to_dest_cell_id_rtis, (start, end))
            for src_cell_id_rti, dest_cell_id_rti, freq in cur_local.fetchall():
                src_dest_cell_id_rti_pair_imsis[
                    (src_cell_id_rti, dest_cell_id_rti)][(start, end)] = freq

    if analyse_src:
        print("Any source spikes?")
        for cell_id_rti, date_freqs in src_cell_id_rti_imsis.items():
            if utils.has_spike(date_freqs.values(), min_factor_times_mean=3):
                print("cell_id_rti: {}".format(cell_id_rti), end=' ')
                pp(date_freqs)    
        with open('oddity_analysis_source_cell_id_rtis.txt', 'w') as f:
            f.write("Source cell id rti's")
            f.write(pf(src_cell_id_rti_imsis))

    if analyse_dest:
        print("Any dest spikes?")
        for cell_id_rti, date_freqs in dest_cell_id_rti_imsis.items():
            if utils.has_spike(date_freqs.values(), min_factor_times_mean=3):
                print("cell_id_rti: {}".format(cell_id_rti), end=' ')
                pp(date_freqs)
        with open('oddity_analysis_dest_cell_id_rtis.txt', 'w') as f:
            f.write("Dest cell id rti's")
            f.write(pf(dest_cell_id_rti_imsis))

    if analyse_src_dest_pairs:
        print("Any src-dest spikes?")
        for cell_id_rti_pair, date_pair_freqs \
                in src_dest_cell_id_rti_pair_imsis.items():
            if utils.has_spike(date_pair_freqs.values(),
                    min_factor_times_mean=3, min_size=50):
                print("cell_id_rti_pair: {}".format(cell_id_rti_pair), end=' ')
                pp(date_pair_freqs)
        with open('oddity_analysis_src_dest_cell_id_rti_pairs.txt', 'w') as f:
            f.write("Source-Dest cell id rti pairs")
            f.write(pf(src_dest_cell_id_rti_pair_imsis))
    print("Finished!")

def spike_from_src_to_dest_cell_id_rti(dates, src_s2_12_index,
        dest_s2_12_index):
    """
    First in day analyses.

    E.g. on 7 Mar 2015 -> 8 a large number of IMSIs in 734 went to 93 but not in
    previous days.

    match on imsi_rti (same person in one set of cell_id_rti's in start and then
    in other set of cell_id_rti's in end)
    """
    unused_con_rem, cur_rem = Pg.get_rem()
    src_cell_id_rtis = DepArea.get_cell_id_rtis_in_s2_12(
        s2_12_index=src_s2_12_index)
    src_cell_id_rtis_clause = Pg.nums2clause(src_cell_id_rtis)
    dest_cell_id_rtis = DepArea.get_cell_id_rtis_in_s2_12(
        s2_12_index=dest_s2_12_index)
    dest_cell_id_rtis_clause = Pg.nums2clause(dest_cell_id_rtis)
    sql_tpl = """\
    SELECT
    src_cell_id_rti,
    dest_cell_id_rti,
      COUNT(*) AS
    freq
    FROM
    (
      SELECT DISTINCT ON (imsi_rti)
      imsi_rti,
        cell_id_rti AS
      src_cell_id_rti
      FROM lbs_agg.fact_rti_summary
      WHERE date = %s
      ORDER BY imsi_rti, start_date_str ASC
    ) AS src
    INNER JOIN
    (
      SELECT DISTINCT ON (imsi_rti)
      imsi_rti,
        cell_id_rti AS
      dest_cell_id_rti
      FROM lbs_agg.fact_rti_summary
      WHERE date = %s
      ORDER BY imsi_rti, start_date_str ASC
    ) AS dest
    USING(imsi_rti)
    WHERE src_cell_id_rti IN {src_cell_id_rtis_clause}
    AND dest_cell_id_rti IN {dest_cell_id_rtis_clause}
    GROUP BY src_cell_id_rti, dest_cell_id_rti
    ORDER BY src_cell_id_rti, dest_cell_id_rti
    """.format(src_cell_id_rtis_clause=src_cell_id_rtis_clause,
        dest_cell_id_rtis_clause=dest_cell_id_rtis_clause)
    date_pairs = utils.dates2pairs(dates)
    for start, end in date_pairs:
        cur_rem.execute(sql_tpl, (start, end))
        data = cur_rem.fetchall()
        print("cell_id_rti details of IMSI_rti's in {} on {} then in {} on {}"
            .format(src_s2_12_index, start, dest_s2_12_index, end))
        print(sum([row['freq'] for row in data]))
        pp(data)

def indiv_cell_id_rti_locations(cur, src_cell_id_rtis, dest_cell_id_rtis):
    cell_id_rtis = src_cell_id_rtis + dest_cell_id_rtis
    dts = [
        #Dates.get_local_datetime(2015, 3, 7),  ## too early
        Dates.get_local_datetime(2016, 6, 7),
        Dates.get_local_datetime(2017, 3, 1),
    ]
    for dt in dts:
        FoliumMap.map_rti_current_locations(cur, cell_id_rtis,
            location_src=conf.CSM_HISTORY, as_at_dt=dt)

def _make_assoc_tblname(lbl, focus_date):
    tblname = "associated_cells_{}_{}".format(lbl, focus_date.replace('-', ''))
    return tblname

def indiv_imsi_cell_id_rtis(con_local, cur_local, src_cell_id_rtis,
        dest_cell_id_rtis, location_src=conf.CSM_HISTORY, make_fresh_src=True):
    """
    If we choose the first in day cell_id_rti for IMSI's we get the spike but
    not if we look at all cell_id_rti's connected to by an IMSI.

    * all connected cell_id_rti's, not just first in day ****
    4,244 IMSI_rti's in West Coast RTO on 2015-03-06 then in Waitaki RTO on 2015-03-07
    4,618 IMSI_rti's in West Coast RTO on 2015-03-07 then in Waitaki RTO on 2015-03-08
    --------
    4,244-->4,618 is no material change

    * first in day only ****
    cell_id_rti details of IMSI_rti's in 734 on 2015-03-06 then in 93 on 2015-03-07
    140
    [[52521, 52012, 14],
     [52521, 52014, 1],
     [52521, 52015, 20],
     [52522, 52012, 15],
     [52522, 52015, 8],
     [52523, 52012, 12],
     [52523, 52015, 11],
     [52524, 52012, 9],
     [52524, 52015, 5],
     [52525, 52012, 7],
     [52525, 52015, 16],
     [52526, 52012, 10],
     [52526, 52015, 12]]
    cell_id_rti details of IMSI_rti's in 734 on 2015-03-07 then in 93 on 2015-03-08
    1718
    [[52521, 52012, 178],
     [52521, 52015, 196],
     [52522, 52012, 107],
     [52522, 52015, 120],
     [52523, 52012, 92],
     [52523, 52015, 89],
     [52524, 52012, 220],
     [52524, 52015, 240],
     [52525, 52012, 111],
     [52525, 52015, 160],
     [52526, 52012, 109],
     [52526, 52015, 96]]
    --------
    140-->1,718 is a spike :-)
    
    So why the difference? Connection data sometimes being associated with the
    wrong cell_id_rti (perhaps a configuration problem?). Or a relocation we
    don't have data for?

    Detectable? Look at IMSI's on day before spike at src location e.g.
    Westport. Which cell_id_rti's did they connect to (look at shortest
    practical time period to reduce likelihood of genuine travel within timespan
    - but can't be too short or no time to connect to multiple cell_id_rti's).
    Do same, but separately, of course, for dest e.g. Otematata. Shouldn't be
    any spread apart a long way
    """
    debug = False
    src_cell_id_rtis_clause = Pg.nums2clause(src_cell_id_rtis)
    dest_cell_id_rtis_clause = Pg.nums2clause(dest_cell_id_rtis)
    d1 = '2015-03-07'
    d2 = '2015-03-08'
    tbl_dets = [
        (d1, src_cell_id_rtis_clause, _make_assoc_tblname('src', d1)),    
        (d1, dest_cell_id_rtis_clause, _make_assoc_tblname('dest', d1)),    
        (d2, src_cell_id_rtis_clause, _make_assoc_tblname('src', d2)),    
        (d2, dest_cell_id_rtis_clause, _make_assoc_tblname('dest', d2)),    
    ]
    if not make_fresh_src:
        utils.warn("Not making fresh associated cell_id_rti's data")
    else:
        unused_con_rem, cur_rem = Pg.get_rem()
        """
        Get all rtis for imsis where imsi has a src rti - then collect rtis with
        high freqs.

        Using string interpolation and not even at gunpoint!
        Build SQL template bit-by-bit using sprintf interpolation so we can
        placeholders for a final .format interpolation at end when we consume
        the final template (simple and avoids brain-exploding nested escaping
        ;-)).

        Need to look at a tight time range but not so tight that an IMSI can't
        be registered against more than one cell_id_rti.
        """
        sql_imsis_in_area_tpl = """\
        SELECT DISTINCT imsi_rti
        FROM lbs_agg.fact_rti_summary
        WHERE date = '{date}'  /* logically redundant but helps select correct date-partitioned table */
        AND start_date_str >= '{ll_datetime}'
        AND end_date_str <= '{ul_datetime}'
        AND cell_id_rti IN {cell_id_rtis_clause}
        """  ## escaped so NOT escaped in after inserted
        sql_all_rtis_for_imsis_in_area_tpl = """\
        SELECT cell_id_rti  /* all cell_id_rti's */
        FROM lbs_agg.fact_rti_summary
        INNER JOIN (
          %(sql_imsis_in_area_tpl)s
        ) AS imsis_in_area /* filter to IMSI's we care about */
        USING(imsi_rti)
        WHERE  date = '{date}'
        AND start_date_str >= '{ll_datetime}'
        AND end_date_str <= '{ul_datetime}'
        """ % {'sql_imsis_in_area_tpl': sql_imsis_in_area_tpl}
        sql_freqs_tpl = """\
        SELECT
        cell_id_rti,
          COUNT(*) AS
        freq
        FROM (
          %(sql_all_rtis_for_imsis_in_area_tpl)s
        ) AS associated_cell_id_rtis
        GROUP BY cell_id_rti
        HAVING COUNT(*) >= 10
        """ % {'sql_all_rtis_for_imsis_in_area_tpl': \
            sql_all_rtis_for_imsis_in_area_tpl}
        for focus_date, clause, tblname in tbl_dets:
            ll_datetime = focus_date + " 16:00:00"
            ul_datetime = focus_date + " 17:00:00"
            sql_freqs = sql_freqs_tpl.format(cell_id_rtis_clause=clause,
                date=focus_date, ll_datetime=ll_datetime,
                ul_datetime=ul_datetime)
            if debug: print(sql_freqs); return;
            cur_rem.execute(sql_freqs)
            Pg.drop_tbl(con_local, cur_local, tblname)
            sql_make_tbl = """\
            CREATE TABLE {tblname} (
              id SERIAL,
              cell_id_rti integer,
              freq integer,
              PRIMARY KEY (id)
            )
            """.format(tblname=tblname)
            cur_local.execute(sql_make_tbl)
            sql_insert_tpl = """\
            INSERT INTO {tblname}
            (cell_id_rti, freq)
            VALUES (%s, %s)
            """.format(tblname=tblname)
            n = 0
            while True:
                row = cur_rem.fetchone()
                if not row:
                    n = 1
                    break
                cur_local.execute(sql_insert_tpl, (row['cell_id_rti'],
                    row['freq']))
                if n % 100 == 0:
                    print("Completed adding {:,} associated cell_id_rti records"
                        .format(n))
                n += 1
            print("Completed adding {:,} associated cell_id_rti records"
                .format(n - 1))
            con_local.commit()
    for focus_date, unused_clause, tblname in tbl_dets:
        sql_rtis = """\
        SELECT cell_id_rti
        FROM {tblname}
        ORDER BY cell_id_rti
        """.format(tblname=tblname)
        cur_local.execute(sql_rtis)
        cell_id_rtis = [row['cell_id_rti'] for row in cur_local.fetchall()]
        if cell_id_rtis:
            dt = None  ## both date ranges too early for location source
            map_url = FoliumMap.map_rti_current_locations(cur_local,
                cell_id_rtis, location_src=location_src, as_at_dt=dt)
            utils.open_map_url(map_url)
        else:
            print("Not making map - no cell_id_rti's to display")

def report_prehistoric_relocations(con_local, cur_local, fpath,
        make_fresh_src=False, audit=False):
    if make_fresh_src:
        locsrcs.make_oracle_wnms_locations(con_local, cur_local, fpath)
    reprels.report_relocations(cur_local, src=conf.ORACLE_WNMS_LOCATIONS,
        prog_feedback_weekly=False, audit=audit)
    

def main():
    skip = True
    con_local, cur_local, unused_cur_local2 = Pg.get_local()
    src_region = 'West Coast RTO'
    dest_region = 'Waitaki RTO'
    dates = [
        '2015-03-05',
        '2015-03-06',
        '2015-03-07',
        '2015-03-08',
        '2015-03-09',
        '2015-03-10',
    ]
    ## focusing on cell_id_rti's in 734 (Westport) or 93 (Otematata) 
    src_cell_id_rtis = [52521, 52522, 52523, 52524, 52526]  ## in Westport and stayed there
    dest_cell_id_rtis = [52012, 52015]  ## shifted from Westport to Otematata (along with 52011 and 52014)
    s2_12_to_region_name = DepArea.get_s2_12_to_region_name(
        pkl='csm_s2_12_maps_20170405.pkl')
    if not skip:
        first_in_day_categorized_analyses(con_local, cur_local, src_region,
            dest_region, s2_12_to_region_name)
        max_by_imsi_35_days_analyses(con_local, cur_local, src_region, dest_region,
            s2_12_to_region_name)
        first_in_day_analyses(con_local, cur_local, src_region, dest_region,
            s2_12_to_region_name, audit=True)
        src_to_dest_cell_id_rti_connection_data(con_local, cur_local, dates,
            src_region, dest_region, s2_12_to_region_name, make_src=False)
        spike_from_src_to_dest_cell_id_rti(dates, src_s2_12_index=734,
            dest_s2_12_index=93)
        indiv_cell_id_rti_locations(cur_local, src_cell_id_rtis,
            dest_cell_id_rtis)
        indiv_imsi_cell_id_rtis(con_local, cur_local, src_cell_id_rtis,
            dest_cell_id_rtis, location_src=conf.CSM_HISTORY,
            make_fresh_src=True)
        src_to_dest_region_connection_data(con_local, cur_local, dates, src_region,
            dest_region, s2_12_to_region_name, make_src=False)
    report_prehistoric_relocations(con_local, cur_local,
        fpath='/home/gps/Documents/tourism/csm/cellid_sites.csv',
        make_fresh_src=True, audit=True)

if __name__ == '__main__':
    main()
