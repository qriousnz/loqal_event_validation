#! /usr/bin/env python3

"""
Any odd patterns in terms of people volumes per area (S2 Level 12 grids)?

Take into account weekends and holidays. Try to use local knowledge of specific
areas to interpret results e.g. popular holiday destinations.

Note - some holiday locations won't have cell towers inside their boundaries
so we might be interested in near-by towers as being relevant to them.

Note - all dates in configuration are strings for easy entry but all dates
inside script are processed and compared as datetime dates.

Mt Albert - City suburb
Waiuku - Rural service town
Whangamata - Summer national holiday destination/baches etc
Raglan - Summer national holiday destination/baches etc
Rotorua - Regional city and all-year national/international tourist destination
Napier - Regional city, some domestic tourism
Palmerston North - Regional city, University town
Wellington - Government CBD - town centre vs higher socio-economic suburb
Queenstown - Winter national/international tourist destination
Franz Josef - National/international tourism destination (limited accommodation). West Coast (unaffected by East Coast earthquake?)
Dunedin - Regional city, University town (town centre vs uni and popular student accommodation)
"""
from collections import OrderedDict
from datetime import datetime, timedelta
import json
from pprint import pprint as pp
from webbrowser import open_new_tab

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import psycopg2 as pg
import psycopg2.extras as pg_extras

import not4git

import sys
sys.path.insert(0, '../voyager_shared')
import conf #@UnresolvedImport
from mpl import Humanise #@UnresolvedImport
import utils #@UnresolvedImport

utils.warn("Still using john_test as source for some data")

MAKE_FRESH_CHARTS = True
REFRESH_RR = False
REFRESH_SECTOR_LOC = False
if not MAKE_FRESH_CHARTS:
    print("\n\nSet MAKE_FRESH_CHARTS to True as soon as finished testing!\n\n")
MAX_NANS = 30
MAIN_IMG_HEIGHT = 25
MAX_LEGEND_ITEMS = 15
X_OFFSET = timedelta(hours=36)
REDROCK_HISTORY = 'redrock_history'
CONNECTION_DATA = 'lbs_agg.fact_rti_summary'
CSM_HISTORY = 'csm'
APPEARED = 'appeared'
DISAPPEARED = 'disappeared'

## open s2_12_map.html to look up s2_12_indexes for specific areas
## Add labels to s2_index_to_label in s2_level_12_mappings.json
## Note -- new s2_12_indexes are added onto end of existing list so if 557 means
## Kaikoura it always will.
FOCUS_AREA_S2_L2_INDEXES = [
    491,  ## Waitangirua (East)
    492,  ## Waitangirua (West)
    577,  ## Kaikoura
    666,  ## South-west of Kaikoura
    526,  ## Clarence (north of Kaikoura)
    11,   ## Queenstown
    53,   ## Dunedin Uni
    55,   ## Dunedin
    143,  ## Whangamata
    259,  ## Riverlea (near Hamilton)
    262,  ## Mystery Creek
    312,  ## Rotorua
    339,  ## Whakapapa
    378,  ## Napier
    421,  ## Palmerston North
    422,  ## Massey Uni
    545,  ## Karori
    549,  ## Wellington
    721,  ## Franz Josef
    773,  ## Raglan
    782,  ## Waiuku
    859,  ## Mt Albert
]

(nat_hol_dts, reg_hol_dts, hol_dt2dets,
 nat_event_dt2dets,
 reg_event_mappings) = Humanise.prep_date_event_mappings()

mappings = json.load(open('./s2_level_12_mappings.json'))
area2lbl = mappings['s2_index_to_label']
area2rto = mappings['s2_index_to_rto']

def _has_extreme_outliers(vals):
    q75, q25 = np.percentile(vals, [75 ,25])
    iqr = q75 - q25
    N_IQR = 15
    upper_bound = q75 + N_IQR*iqr
    lower_bound = q25 - N_IQR*iqr
    extreme_outliers = False
    for val in vals:
        if not lower_bound < val < upper_bound:
            extreme_outliers = True
            break
    return extreme_outliers

def is_interesting(s2_12_idx, daily_data):
    """
    Any values extremely low or high compared to others? nan is automatically
    considered extreme.

    Compares weekend days with each other and non-holiday weekdays. Holiday
    weekends are already too extreme to include when looking for outliers.

    daily_data e.g.

    2016-06-01    2425.0
    2016-06-02    2568.0
                 ...  
    2016-11-13    2154.0
    2016-11-14    1895.0
    """
    debug = False
    weekend_vals = []
    non_holiday_weekday_vals = []
    for data_dic in daily_data:
        if data_dic['weekend']:
            weekend_vals.append(data_dic['freq'])
        else:
            if not (data_dic['reg_hol'] or data_dic['nat_hol']):
                non_holiday_weekday_vals.append(data_dic['freq'])
    nans = pd.isnull(weekend_vals + non_holiday_weekday_vals)
    n_nans = list(nans).count(True)
    if debug: print(n_nans)
    excess_nans = n_nans > MAX_NANS
    interesting = (excess_nans
        or _has_extreme_outliers(weekend_vals)
        or _has_extreme_outliers(non_holiday_weekday_vals))
    return interesting

def make_image(s2_12_idx, daily_data, img_path):
    fig = plt.figure(figsize=(180, MAIN_IMG_HEIGHT))
    ax = fig.add_subplot(111)
    x = [data_dic['dt'] for data_dic in daily_data]
    y = [data_dic['freq'] for data_dic in daily_data]
    min_x = min(x)
    max_y = max(y)
    rto = area2rto.get(str(s2_12_idx))
    humanised = Humanise.annotate_and_colour(daily_data, rto,
        nat_event_dt2dets, reg_event_mappings, hol_dt2dets)
    annotation_y_val = max_y/50
    if(pd.isnull(annotation_y_val)): annotation_y_val = 1
    (std_offset, small_std_offset,
     tiny_std_offset) = utils.Mpl.get_y_offsets(max_y)
    ax.bar(x, y, 0.5, color=humanised['colours4dts'], align='center')
    ## Annotations and annotation legend
    legend_x_offset = 2*X_OFFSET
    col_x_offset = legend_x_offset + (2*X_OFFSET)
    utils.Mpl.add_annotation_legend(ax, legend_x_offset, col_x_offset, min_x,
        max_y, small_std_offset, tiny_std_offset)
    utils.Mpl.add_mpl_annotations(ax, humanised['dts_with_annotations'],
        X_OFFSET, std_offset, small_std_offset, tiny_std_offset)
    ## other
    ax.set_ylim(ymin=0)
    utils.Mpl.set_ticks(ax)
    utils.Mpl.set_axis_labels(ax)
    title = area2lbl.get(str(s2_12_idx), 'S2 Level 12 id {}'.format(s2_12_idx))
    utils.Mpl.set_titles(ax, title)
    plt.xlabel('Date')
    plt.ylabel('N. devices')
    plt.savefig(img_path, bbox_inches='tight')
    plt.close(fig)

def _process_chart(html, s2_12_idx, daily_data):
    html_rel_img_path = 'images/charts/{}.png'.format(s2_12_idx)
    fs_rel_img_path = 'reports/' + html_rel_img_path
    if MAKE_FRESH_CHARTS:
        make_image(s2_12_idx, daily_data, fs_rel_img_path)
    html.append("<img width='3200px' padding=0 margin=0 src='{}'>".format(
        html_rel_img_path))
    print("Processed '{}'".format(s2_12_idx))

def _get_rem():
    pwd_rem = not4git.access_rem  #getpass.getpass('Enter remote postgres password: ')
    con_rem = pg.connect("dbname='tnz_loc' "
        "user='gpadmin' "
        "host='10.100.99.121' "
        "port=5432 "
        "password='{}'".format(pwd_rem))
    cur_rem = con_rem.cursor(cursor_factory=pg_extras.DictCursor)
    return con_rem, cur_rem

def get_where_cell_id_rtis_in_s2_12(s2_12_index):
    """
    Other cell_id_rti's might have been associated with this s2_12 in the past.
    May need to extract a more complete list.
    Trying to work out which cell_id_rti's to use by date is a much, much more
    difficult task and much harder to audit.
    """
    unused, cur_rem = _get_rem()
    sql_cell_id_rtis = """\
    SELECT cell_id_rti
    FROM tourism_input.csm_cell_id_to_s2_12_index
    WHERE s2_12_index = %s
    ORDER BY cell_id_rti
    """.format()
    cur_rem.execute(sql_cell_id_rtis, (s2_12_index, ))
    cell_id_rti_strings = [str(row['cell_id_rti'])
        for row in cur_rem.fetchall()]
    where = "WHERE cell_id_rti IN ({})".format(', '.join(cell_id_rti_strings))
    return where

def _make_fresh_src_by_rti(cur_rem, con_local, cur_local,
        where_cell_id_rtis_in_s2_12, tbl_by_rti):
    sql_by_rti = """\
    SELECT
    date,
    cell_id_rti,
      COUNT(*) AS
    freq
    FROM (
      SELECT DISTINCT date, cell_id_rti, imsi_rti
      FROM {connection_data}
      {where_cell_id_rtis_in_s2_12}
    ) AS imsi_agg /* multiple records per date - cell_id_rti - imsi_rti even though events grouped */
    GROUP BY date, cell_id_rti
    ORDER BY date, cell_id_rti
    """.format(connection_data=CONNECTION_DATA,
        where_cell_id_rtis_in_s2_12=where_cell_id_rtis_in_s2_12)
    cur_rem.execute(sql_by_rti)
    data_by_rti = cur_rem.fetchall()
    utils.Pg.drop_tbl(con_local, cur_local, tbl=tbl_by_rti)
    sql_make_tbl_by_rti = """\
    CREATE TABLE {tbl_by_rti} (
      id SERIAL,
      date date,
      cell_id_rti integer,
      freq integer,
      PRIMARY KEY(id)
    )
    """.format(tbl_by_rti=tbl_by_rti)
    cur_local.execute(sql_make_tbl_by_rti)
    con_local.commit()
    sql_insert_by_rti_tpl = """\
    INSERT INTO {tbl_by_rti}
    (date, cell_id_rti, freq)
    VALUES (%s, %s, %s)
    """.format(tbl_by_rti=tbl_by_rti)
    for n, row in enumerate(data_by_rti, 1):
        date, cell_id_rti, freq = row
        cur_local.execute(sql_insert_by_rti_tpl, (date, cell_id_rti, freq))
        if n % 500 == 0:
            print("Added {:,} records to {}".format(n, tbl_by_rti))
    con_local.commit()

def _make_fresh_src_tot(cur_rem, con_local, cur_local,
        where_cell_id_rtis_in_s2_12, tbl_tot):
    utils.Pg.drop_tbl(con_local, cur_local, tbl=tbl_tot)
    sql_tot = """\
    SELECT
    date,
      COUNT(*) AS
    freq
    FROM ( 
      SELECT DISTINCT date, IMSI_rti
      FROM {connection_data}
      {where_cell_id_rtis_in_s2_12}
    ) AS unique_imsis
    GROUP BY date
    ORDER BY date
    """.format(connection_data=CONNECTION_DATA,
        where_cell_id_rtis_in_s2_12=where_cell_id_rtis_in_s2_12)
    cur_rem.execute(sql_tot)
    data_tot = cur_rem.fetchall()
    sql_make_tbl_tot = """\
    CREATE TABLE {tbl_tot} (
      id SERIAL,
      date date,
      freq integer,
      PRIMARY KEY(id)
    )
    """.format(tbl_tot=tbl_tot)
    cur_local.execute(sql_make_tbl_tot)
    con_local.commit()
    sql_insert_tot_tpl = """\
    INSERT INTO {tbl_tot}
    (date, freq)
    VALUES (%s, %s)
    """.format(tbl_tot=tbl_tot)
    for n, row in enumerate(data_tot, 1):
        date, freq = row
        cur_local.execute(sql_insert_tot_tpl, (date, freq))
        if n % 500 == 0:
            print("Added {:,} records to {}".format(n, tbl_tot))
    con_local.commit()

def _make_fresh_src(s2_12_index, tbl_by_rti, tbl_tot):
    skip_by_rti = False
    unused, cur_rem = _get_rem()
    where_cell_id_rtis_in_s2_12 = get_where_cell_id_rtis_in_s2_12(s2_12_index)
    if skip_by_rti:
        utils.warn("Skipping making the by_rti table")
    else:
        _make_fresh_src_by_rti(cur_rem, con_local, cur_local,
            where_cell_id_rtis_in_s2_12, tbl_by_rti)
    _make_fresh_src_tot(cur_rem, con_local, cur_local,
        where_cell_id_rtis_in_s2_12, tbl_tot)
    print("Finished making '{tbl_by_rti}' and '{tbl_tot}'".format(
        tbl_by_rti=tbl_by_rti, tbl_tot=tbl_tot))

def _get_imsis(cur_rem, where_cell_id_rtis_in_s2_12, datestr):
    debug = False
    sql_imsis = """\
    SELECT DISTINCT
    IMSI_rti
    FROM {connection_data}
    {where_cell_id_rtis_in_s2_12}
    AND date = %s
    ORDER BY IMSI_rti
    """.format(connection_data=CONNECTION_DATA,
        where_cell_id_rtis_in_s2_12=where_cell_id_rtis_in_s2_12)
    if debug: print(sql_imsis)
    cur_rem.execute(sql_imsis, (datestr, ))
    imsis = [row[0] for row in cur_rem.fetchall()]
    return imsis

def _compare_imsis(imsis_1, imsis_2):
    debug=False
    tot = len(set(imsis_1).union(set(imsis_2)))
    both = set(imsis_1).intersection(set(imsis_2))
    both_n = len(both)
    both_pct = round(100*(both_n/tot), 1)
    dt1_only = set(imsis_1).difference(set(imsis_2))
    dt1_only_n = len(dt1_only)
    dt1_only_pct = round(100*(dt1_only_n/tot), 1)
    dt2_only = set(imsis_2).difference(set(imsis_1))
    dt2_only_n = len(dt2_only)
    dt2_only_pct = round(100*(dt2_only_n/tot), 1)
    if debug:
        pp("\n\ndt1:\n{}".format(sorted(list(dt1_only))))
        pp("\n\ndt2:\n{}".format(sorted(list(dt2_only))))
    return both_n, both_pct, dt1_only_n, dt1_only_pct, dt2_only_n, dt2_only_pct


def report_IMSI_churn(cur_rem, where_cell_id_rtis_in_s2_12, datestr_1,
        datestr_2):
    """
    Comparison of two dates so we can see whether the change is about adding
    IMSI's, losing some, or a completely different set of IMSI's, or something
    else ;-).
    """
    imsis = []
    for datestr in [datestr_1, datestr_2]:
        imsis.append(_get_imsis(cur_rem, where_cell_id_rtis_in_s2_12, datestr))
    return (datestr_1, datestr_2, *_compare_imsis(*imsis))

def report_comparative_IMSI_churn(s2_12_index, date_pair_where_changing,
        usual_date_pairs):
    """
    Results for two comparisons - a comparison across an interesting date
    boundary e.g. where total IMSI's increases considerably, and the results of
    a more typical comparison of two dates.
    """
    unused, cur_rem = utils.get_rem()
    where_cell_id_rtis_in_s2_12 = get_where_cell_id_rtis_in_s2_12(s2_12_index)
    results_where_change = report_IMSI_churn(cur_rem,
        where_cell_id_rtis_in_s2_12, *date_pair_where_changing)
    usual_results = []
    for date_pair in usual_date_pairs:
        usual_results.append(report_IMSI_churn(cur_rem,
        where_cell_id_rtis_in_s2_12, *date_pair))
    return results_where_change, usual_results

def report_changes_by_cell_id_rti(con_local, cur_local, s2_12_index, lbl='',
        make_fresh_src=False, special_cell_id_rtis=None):
    """
    Stacked bar chart focusing on what individual cell_id_rti's contribute to
    the total picture for an s2 level 12 area.
    """
    summary = """\
    Data source - lbs_agg.fact_rti_summary Filter: cell_id_rti's
    for selected s2_12_index as mapped by latest
    tourism_input.csm_cell_id_to_s2_12_index table. Plotting sum of devices
    (IMSI's) per day per cell_id_rti.
    
    Here we focus on a specific s2 level 12 area. We look at all cell_id_rti's
    associated with that area. We look at the combined contribution of all these
    cell_id_rti's to the total and also a breakdown. Note the total double count
    IMSI's when they connect to more than one cell_id_rti which is why the total
    is displayed as a line. Although we expect to see individual cell_id_rti's
    joining and exiting the system we do not expect the total to significantly
    change unless there is a change of technologies which significantly
    increases coverage. The breakdown is so we can see if individual
    cell_id_rti's are particularly implicated in any discontinuities etc.
    """
    if not special_cell_id_rtis:
        special_cell_id_rtis = []
    test_rtis = [] #[282636, 28202, 35771, ]
    date_range = [] #[dt_date(2016,7,28), dt_date(2016,10,5)]
    if test_rtis:
        utils.warn("Stop using test RTIs!!!!!!!!!!!")
    if date_range:
        utils.warn("Stop using date range!!!!!!!!!!!")
    debug = False
    tbl_by_rti = "s2_12_cell_id_rti_daily_{:06}".format(s2_12_index)  ## approx 135000 s2_12_indexes ever if whole country covered so 6 chars is enough
    tbl_tot = "s2_12_daily_{:06}".format(s2_12_index)  ## approx 135000 s2_12_indexes ever if whole country covered so 6 chars is enough
    if not make_fresh_src:
        utils.warn("Not making fresh source data for {}".format(s2_12_index))
    else:
        if not test_rtis:
            _make_fresh_src(s2_12_index, tbl_by_rti, tbl_tot)

    ## generate data
    sql_get_data_by_rti = """\
    SELECT *
    FROM {tbl_by_rti}
    """.format(tbl_by_rti=tbl_by_rti)
    cur_local.execute(sql_get_data_by_rti)
    data_by_rti = cur_local.fetchall()
    if debug: pp(data_by_rti)
    sql_dates = """\
    SELECT DISTINCT date
    FROM {tbl_by_rti}
    ORDER BY date
    """.format(tbl_by_rti=tbl_by_rti)
    cur_local.execute(sql_dates)
    dates = [row['date'] for row in cur_local.fetchall()]
    if date_range:
        start, end = date_range
        dates = [date for date in dates if start <= date <= end]
    if debug: print(dates)
    if not test_rtis:
        sql_cell_id_rtis = """\
        SELECT
        cell_id_rti,
          SUM(freq) AS
        total
        FROM {tbl_by_rti}
        GROUP BY cell_id_rti
        ORDER BY cell_id_rti
        """.format(tbl_by_rti=tbl_by_rti)
        cur_local.execute(sql_cell_id_rtis)
        cell_id_rti_data = cur_local.fetchall()
        cell_id_rti_data.sort(key=lambda s: s['total'], reverse=True)  ## so we give the best colours to the most important cell_id_rti's
        cell_id_rtis = [row['cell_id_rti'] for row in cell_id_rti_data]
    else:
        cell_id_rtis = special_cell_id_rtis = test_rtis
    ## make line data for total so can see unique IMSI total
    sql_get_data_tot = """\
    SELECT *
    FROM {tbl_tot}
    """.format(tbl_tot=tbl_tot)
    cur_local.execute(sql_get_data_tot)
    data_tot = cur_local.fetchall()
    if debug: pp(data_tot)

    ## make MAIN chart
    ## create easier data structure with keys of cell_id_rti then date
    dic_by_rti = {(row['cell_id_rti'], row['date']): row['freq']
        for row in data_by_rti}
    width_scalar = len(dates)/200 if date_range else 1
    width = width_scalar*180
    fig_main = plt.figure(figsize=(width, MAIN_IMG_HEIGHT*0.7))
    ax_main = fig_main.add_subplot(111)
    lbl = lbl + ' ' if lbl else ''
    main_chart_title = ("{}S2 level 12 area {} by cell_id_rti contribution"
        .format(lbl, s2_12_index))
    utils.Mpl.set_titles(ax_main, main_chart_title)
    utils.Mpl.set_ticks(ax_main)
    utils.Mpl.set_axis_labels(ax_main, y_label='N unique IMSIs')
    x_main = dates
    dic_tot = {row['date']: row['freq'] for row in data_tot}
    y_tot = [dic_tot.get(date, 0) for date in dates]  ## need a value for every date
    if debug: pp(y_tot)
    if not test_rtis:
        ## put special cases first (i.e. at bottom of stacks)
        if len(cell_id_rtis) != len(set(cell_id_rtis)):
            raise Exception("Duplicates in cell_id_rtis")
        for spec_id in special_cell_id_rtis:
            cell_id_rtis.remove(spec_id)
        cell_id_rtis = special_cell_id_rtis + cell_id_rtis
    legends_dets = []
    cumulative_ys = []
    colours = Humanise.colours(colour_filter=Humanise.COLOURS_ALL)
    ## get freshest band data from location sector history
    sql_get_band = """\
    SELECT cell_id_rti, band
    FROM {sector_location_history}
    WHERE as_at_datetime = (
      SELECT MAX(as_at_datetime)
      FROM {sector_location_history})
    """.format(sector_location_history=conf.SECTOR_LOCATION_HISTORY)
    cur_local.execute(sql_get_band)
    id2band = dict(cur_local.fetchall())
    for cell_id_rti in cell_id_rtis:
        ## y values
        y_by_rti = [dic_by_rti.get((cell_id_rti, date), 0) for date in dates]  ## need a value for every date
        if debug: print(y_by_rti)
        ## bottom y position to stack on top of - is starting y-coord for each bar at x-coord
        if not cumulative_ys:
            bottom = None
            cumulative_ys = y_by_rti
        else:
            bottom = cumulative_ys
            cumulative_ys = [a+b for a, b in zip(cumulative_ys, y_by_rti)]  ## yes - could have used numpy but couldn't remember the syntax and not large volume of data ;-)
        ## colours
        color=next(colours)
        ## actual bar
        ax_bar = ax_main.bar(x_main, y_by_rti, width=0.9, color=color,
            bottom=bottom, align='center')
        ## legend
        if len(legends_dets) <= MAX_LEGEND_ITEMS:
            band = id2band.get(cell_id_rti)
            band_lbl = " ({})".format(band) if band else ''
            legends_dets.append(
                {'leg_item': ax_bar,
                 'lbl': "cell_id_rti: {}{}".format(cell_id_rti, band_lbl),
            })
    max_main_y = max(cumulative_ys)
    (std_main_offset, small_std_main_offset,
     tiny_std_main_offset) = utils.Mpl.get_y_offsets(max_main_y)
    raw_daily_main_data = [{'date': date, 'freq': freq}
        for date, freq in zip(x_main, cumulative_ys)]
    daily_main_data = Humanise.enrich_dates(raw_daily_main_data,
        nat_event_dt2dets, nat_hol_dts, reg_hol_dts)
    rto = area2rto.get(str(s2_12_index))
    humanised_main_data = Humanise.annotate_and_colour(daily_main_data,
        rto, nat_event_dt2dets, reg_event_mappings, hol_dt2dets, max_main_y)
    ## Annotations and annotation legend
    legend_x_offset = 14*X_OFFSET  ## need to sidestep the cell_id_rti legend
    col_x_offset = legend_x_offset + (2*X_OFFSET)
    utils.Mpl.add_annotation_legend(ax_main, legend_x_offset, col_x_offset,
        min(dates), small_std_main_offset, tiny_std_main_offset)
    utils.Mpl.add_mpl_annotations(ax_main,
        humanised_main_data['dts_with_annotations'], X_OFFSET, std_main_offset,
        small_std_main_offset, tiny_std_main_offset)
    handles = [legend_dets['leg_item'] for legend_dets in legends_dets]
    ax_line, = ax_main.plot(x_main, y_tot, linewidth=5, color='#e1e1e1')  ## http://stackoverflow.com/questions/11983024/matplotlib-legends-not-working
    lbls = [legend_dets['lbl'] for legend_dets in legends_dets]
    handles.insert(0, ax_line)
    lbls.insert(0, "TOTAL IMSI's")
    utils.Mpl.set_legends(ax_main, handles, lbls)
    utils.Mpl.add_year_vlines(ax_main, X_OFFSET, x_main, rotate=True,
        lower_propn_line=0.92, lower_propn_text=0.99, fontsize=24)
    html_rel_main_img_path = ('images/charts/{}_by_cell_id_index.png'
        .format(s2_12_index))
    fs_rel_main_img_path = 'reports/' + html_rel_main_img_path        
    #plt.show(); return
    plt.savefig(fs_rel_main_img_path, bbox_inches='tight')
    plt.close(fig_main)
    title_main = ("{}{} Contribution of individual cell_id_rti's to "
        "discontinuities in total devices connected to per day"
        .format(lbl, s2_12_index))
    html = [conf.HTML_START_TPL % {'title': title_main, 'more_styles': ''},
        "<p>{}</p>".format(summary),
    ]
    html.append("<img width='3200px' padding=0 margin=0 src='{}'>".format(
        html_rel_main_img_path))

    ## make secondary chart
    x_tot = dates
    max_tot_y = max(y_tot)
    (std_tot_offset, small_std_tot_offset,
     tiny_std_tot_offset) = utils.Mpl.get_y_offsets(max_tot_y)
    raw_daily_tot_data = [{'date': date, 'freq': freq}
        for date, freq in zip(x_tot, y_tot)]
    daily_tot_data = Humanise.enrich_dates(raw_daily_tot_data,
        nat_event_dt2dets, nat_hol_dts, reg_hol_dts)
    humanised_tot_data = Humanise.annotate_and_colour(daily_tot_data, rto,
        nat_event_dt2dets, reg_event_mappings, hol_dt2dets, max_tot_y)
    fig_tot = plt.figure(figsize=(width, MAIN_IMG_HEIGHT*0.7))
    ax_tot = fig_tot.add_subplot(111)
    tot_chart_title = "{}S2 level 12 area {}".format(lbl, s2_12_index)
    utils.Mpl.set_titles(ax_tot, tot_chart_title)
    utils.Mpl.set_ticks(ax_tot)
    utils.Mpl.set_axis_labels(ax_tot, y_label='N unique IMSIs')
    ax_tot.bar(x_tot, y_tot, width=0.9, color='#e1e1e1', align='center')
    utils.Mpl.add_year_vlines(ax_tot, X_OFFSET, x_tot, rotate=True,
        lower_propn_line=0.92, lower_propn_text=0.99, fontsize=24)
    utils.Mpl.add_annotation_legend(ax_tot, legend_x_offset, col_x_offset,
        min(dates), small_std_tot_offset, tiny_std_tot_offset)
    utils.Mpl.add_mpl_annotations(ax_tot,
        humanised_tot_data['dts_with_annotations'], X_OFFSET, std_tot_offset,
        small_std_tot_offset, tiny_std_tot_offset)
    html_rel_tot_img_path = ('images/charts/{}{}_tot.png'
        .format(lbl, s2_12_index))
    fs_rel_tot_img_path = 'reports/' + html_rel_tot_img_path        
    #plt.show(); return
    plt.savefig(fs_rel_tot_img_path, bbox_inches='tight')
    plt.close(fig_tot)
    html.append("<img width='3200px' padding=0 margin=0 src='{}'>".format(
        html_rel_tot_img_path))

    ## assemble
    content = "\n".join(html)
    fpath = ("/home/gps/Documents/tourism/validation/reports/"
        "s2_12_{}_breakdown.html".format(s2_12_index))
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))

def report_sector_changes_for_s2_12(cur_local, s2_12_index, src=CSM_HISTORY):
    """
    Show when cell_id_rti's associated with given s2_12_index first appeared and
    finally disappeared (for good).

    Also shows whether the cell_id_rti's significantly changed location.

    Note - assumes that the mapping to s2_12 in the latest
    csm_cell_id_to_s2_12_index table applies to all times even though it won't
    and some cell_id_rti's will have been mapped differently in the past.
    """
    debug = False
    where_cell_id_rtis_in_s2_12 = get_where_cell_id_rtis_in_s2_12(s2_12_index)
    ## appearances/disappearances
    fldnames = utils.LocationSources.get_fldnames(src)
    sql_start_end = """\
    SELECT
      MIN({dt_field}) AS
    start,
      MAX({dt_field}) AS
    end
    FROM {src}
    """.format(src=src, dt_field=fldnames['dt_field'])
    cur_local.execute(sql_start_end)
    start, end = cur_local.fetchone()
    sql_appear_disappear = """\
    SELECT *
    FROM (
      SELECT DISTINCT ON (cell_id_rti)
      cell_id_rti,
      {dt_field},
        '{appeared}'::text AS
      change
      FROM {src}
      {where_cell_id_rtis_in_s2_12}
      ORDER BY cell_id_rti, {dt_field} ASC  /* earliest i.e. first when asc sort order */
    ) AS firsts
    UNION ALL
    SELECT *
    FROM
    (
      SELECT DISTINCT ON (cell_id_rti)
      cell_id_rti,
      {dt_field},
        '{disappeared}'::text AS
      change
      FROM {src}
      {where_cell_id_rtis_in_s2_12}
      ORDER BY cell_id_rti, {dt_field} DESC  /* latest i.e. last when desc sort order */
    ) AS lasts
    ORDER BY {dt_field}, cell_id_rti, change
    """.format(where_cell_id_rtis_in_s2_12=where_cell_id_rtis_in_s2_12,
        src=src, dt_field=fldnames['dt_field'], appeared=APPEARED,
        disappeared=DISAPPEARED)
    cur_local.execute(sql_appear_disappear)
    change_data = []
    for row in cur_local.fetchall():
        if row['change'] == APPEARED:
            change = ('appeared' if row[fldnames['dt_field']] != start
                else 'there at start')
        else:
            change = ('disappeared' if row[fldnames['dt_field']] != end
                else 'there at end')
        change_data.append("{date} - {cell_id_rti} {change}".format(
            date=datetime.strftime(row[fldnames['dt_field']], '%Y-%m-%d'),
            change=change,
            cell_id_rti=row['cell_id_rti'])
        )
    if debug: pp(change_data)
    ## relocations
    sql_relocations = """\
    SELECT
    cell_id_rti,
    {tower_lat},
    {tower_lon}
    FROM {src}
    WHERE cell_id_rti IN(
      SELECT cell_id_rti
      FROM (
        SELECT DISTINCT
        cell_id_rti,
        round({tower_lat}::numeric, 4),  /* because precision differences are not real differences */
        round({tower_lon}::numeric, 4)
        FROM {src}
        {where_cell_id_rtis_in_s2_12}
      ) AS coords
      GROUP BY cell_id_rti
      HAVING COUNT(*) > 1
    )
    """.format(where_cell_id_rtis_in_s2_12=where_cell_id_rtis_in_s2_12,
        src=src, tower_lat=fldnames['tower_lat'],
        tower_lon=fldnames['tower_lon'])
    cur_local.execute(sql_relocations)
    relocations_data = cur_local.fetchall()
    if debug:
        print("Relocations: ")
        pp(relocations_data)

def chart_interesting_s2_12s(cur):
    """
    Add weekend, holiday, and location label data to charts where possible
    to make it easier to check face validity.

    Only produce maps for areas that are either well-understood OR are very
    strange in some way.
    """
    title = 'Events by S2 Level 12 Area'
    html = [
        conf.HTML_START_TPL % {'title': title, 'more_styles': ''},
        """<p>
        Displays the data the Voyager 2 results are based on. The source is the
        first_in_day results taken from john_test. These are not a measure of
        visits, vistors, or day trips, but give a sanity check on population in
        morning for each s2 level 12 by contributing cell_id_rti. It shouldn't
        have any significant gaps or discontinuities e.g. major step changes up
        or down. Such issues indicate a problem. We are already at a higher
        level than individual towers so random noise should already be
        "aggregated out". Any gaps/discontinuities etc need an explanation if we
        are to have confidence on what is built on top of them.
        </p>""",
    ]
    focus_charts = OrderedDict()
    interesting_charts = OrderedDict()
    ## give us this day our daily data ...
    sql_s2_12_idxs = """\
    SELECT DISTINCT s2_12_index
    FROM {s2_12_freqs}
    ORDER BY s2_12_index
    """.format(s2_12_freqs=conf.S2_12_FREQS)
    cur.execute(sql_s2_12_idxs)
    s2_12_idxs = [row['s2_12_index'] for row in cur.fetchall()]
    for s2_12_idx in s2_12_idxs:
        sql_data = """\
        SELECT
        date,
          n_sims AS
        freq
        FROM {s2_12_freqs}
        WHERE s2_12_index = %s
        """.format(s2_12_freqs=conf.S2_12_FREQS)
        cur.execute(sql_data, (s2_12_idx, ))
        raw_daily_data = cur.fetchall()
        daily_data = Humanise.enrich_dates(raw_daily_data, nat_event_dt2dets,
            nat_hol_dts, reg_hol_dts)
        if s2_12_idx in FOCUS_AREA_S2_L2_INDEXES:
            focus_charts[s2_12_idx] = daily_data
        if (is_interesting(s2_12_idx, daily_data)
                and s2_12_idx not in focus_charts):
            interesting_charts[s2_12_idx] = daily_data
        should_report = (s2_12_idx in focus_charts
            or s2_12_idx in interesting_charts)
        if should_report:
            print("Will report on {}".format(s2_12_idx))
    if focus_charts:
        html.append("<h2>Focus Charts</h2>")
        ## retain original order
        for s2_12_idx in FOCUS_AREA_S2_L2_INDEXES:
            daily_data = focus_charts[s2_12_idx]
            _process_chart(html, s2_12_idx, daily_data)
    if interesting_charts:
        html.append("<h2>Other Charts of Interest (Extreme Values etc)</h2>")
        for s2_12_idx, daily_data in interesting_charts.items():
            _process_chart(html, s2_12_idx, daily_data)
    content = "\n".join(html)
    fpath = "/home/gps/Documents/tourism/validation/reports/s2_12_events.html"
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))


if __name__ == '__main__':
    con_local, cur_local, cur_local2 = utils.Pg.get_local()

#    utils.LocationSources.get_first_in_day_data(con_local, cur_local, total_refresh=True)
#    report_sector_changes_for_s2_12(cur_local, s2_12_index=310, src=CSM_HISTORY)
    if REFRESH_RR:
        utils.LocationSources.make_redrock_history(con_local, cur_local,
            extract_trucall_zips=True)
    else:
        utils.warn("Not refreshing Redrock data")
    if REFRESH_SECTOR_LOC:
        utils.LocationSources.make_sector_locations_history(con_local,
            cur_local, cur_local2)
    else:
        utils.warn("Not refreshing Sector Location History")
    check_changes = [
#           (11, "Queenstown"),
#           (93, 'Otematata'),  ## http://s2map.com/#order=latlng&mode=polygon&s2=false&points=12117637728967852032
#           (100, 'West of Oamaru'),
#           (143, 'Whangamata'),
#           (259, 'Riverlea (Hamilton suburb)'),
#           (262, "Mystery Creek (Hamilton, site of A&P Show)"),
#           (310, 'North of Rotorua'),
#           (339, "Whakapapa (Ski Field)"),
#           (378, 'Napier'),
#           (473, ''),
            (491,  'Waitangirua (East)'),
            (492,  'Waitangirua (West)'),
#           (526, "Clarence (north of Kaikoura)"),
#           (577, 'Kaikoura'),
#           (666, "South-west of Kaikoura"),
#           (734, 'West Westport'),  ## http://s2map.com/#order=latlng&mode=polygon&s2=false&points=7864698990061682688
#           (859, 'Mt Albert'),
    ]
    for idx, lbl in check_changes:
        report_changes_by_cell_id_rti(con_local, cur_local, s2_12_index=idx,
            lbl=lbl, make_fresh_src=True)
    check_churn = [
#        (259, ('2016-10-25', '2016-10-26'), [('2016-09-18', '2016-09-19'), ('2016-11-24', '2016-11-25')]),  ## before and after change
    ]
    for s2_12_index, date_pair_where_changing, usual_date_pairs in check_churn:
        results_where_change, usual_results = report_comparative_IMSI_churn(
            s2_12_index, date_pair_where_changing, usual_date_pairs)
        print(("IMSI churn results when change in total IMSI's ({} cf {})"
            "\nboth: {:,} ({}%)"
            "\nfirst date only: {:,} ({}%)"
            "\nsecond date only: {:,} ({}%)").format(*results_where_change))
        for usual_result in usual_results:
            print(("IMSI churn results when no change in total IMSI's ({} cf {})"
                "\nboth: {:,} ({}%)"
                "\nfirst date only: {:,} ({}%)"
                "\nsecond date only: {:,} ({}%)").format(*usual_result))
    #chart_interesting_s2_12s(cur_local)
